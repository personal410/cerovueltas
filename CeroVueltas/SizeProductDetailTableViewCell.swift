//
//  SizeProductDetailTableViewCell.swift
//  CeroVueltas
//
//  Created by victor salazar on 27/08/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class SizeProductDetailTableViewCell:UITableViewCell{
    @IBOutlet weak var nameSizeLbl:UILabel!
    @IBOutlet weak var stockSizeLbl:UILabel!
}