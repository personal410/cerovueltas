//
//  UserTabBarViewController.swift
//  CeroVueltas
//
//  Created by victor salazar on 13/08/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class UserTabBarViewController:UITabBarController, UITabBarControllerDelegate {
    override func viewDidLoad(){
        super.viewDidLoad()
        self.delegate = self
    }
    func tabBarController(tabBarController:UITabBarController, shouldSelectViewController viewController:UIViewController) -> Bool {
        if Client.getClient() == nil {
            if self.viewControllers!.indexOf(viewController)! == 1 || self.viewControllers!.indexOf(viewController)! == 3 {
                ToolBox.showNoAccountMessageInViewCont(self)
                return false
            }
        }
        if let navViewCont = viewController as? UINavigationController {
            navViewCont.viewControllers[0].setValue(true, forKey: "reloadData")
        }
        return true
    }
}