//
//  Product+CoreDataProperties.swift
//  CeroVueltas
//
//  Created by victor salazar on 31/08/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//
import Foundation
import CoreData
extension Product{
    @NSManaged var age:String?
    @NSManaged var brand:String?
    @NSManaged var categoryId:NSNumber
    @NSManaged var characteristics:String?
    @NSManaged var gender:String?
    @NSManaged var id:NSNumber
    @NSManaged var name:String?
    @NSManaged var photoURL:String?
    @NSManaged var price:NSNumber
    @NSManaged var productDescription:String?
    @NSManaged var storeId:NSNumber
    @NSManaged var tags:String?
    @NSManaged var category:Category?
    @NSManaged var colors:NSOrderedSet?
    @NSManaged var photos:NSOrderedSet?
    @NSManaged var sizes:NSOrderedSet?
    @NSManaged var locals:NSOrderedSet?
    @NSManaged var store:Store?
}
