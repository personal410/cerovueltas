//
//  ServiceConnector.swift
//  CeroVueltas
//
//  Created by victor salazar on 6/08/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import Foundation
class ServiceConnector{
    class func connectToUrl(url:String, method:String = "POST", params:AnyObject? = nil, response:((AnyObject?, NSError?) -> Void)){
        let req = NSMutableURLRequest(URL: NSURL(string: url)!, cachePolicy: .UseProtocolCachePolicy, timeoutInterval: 30)
        req.HTTPMethod = method
        if params != nil {
            if let dicParams = params as? NSDictionary {
                do{
                    req.HTTPBody = try NSJSONSerialization.dataWithJSONObject(dicParams, options: NSJSONWritingOptions())
                } catch let error as NSError {
                    response(nil, error)
                    return
                }
            }else if let strParams = params as? String {
                req.HTTPBody = strParams.dataUsingEncoding(NSUTF8StringEncoding)
            }
        }
        let task = NSURLSession.sharedSession().dataTaskWithRequest(req, completionHandler: {data, res, error -> Void in
            dispatch_async(dispatch_get_main_queue(), {() in
                SVProgressHUD.dismiss()
            })
            var finalError:NSError? = error
            if finalError == nil {
                var result:AnyObject? = nil
                do{
                    result = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments)
                } catch let jsonError as NSError {
                    let stringTemp = String(data: data!, encoding: NSUTF8StringEncoding)
                    print("stringTemp: \(stringTemp)")
                    result = nil
                    finalError = jsonError
                }
                dispatch_async(dispatch_get_main_queue(), {() in
                    SVProgressHUD.dismiss()
                    response(result, finalError)
                })
            }else{
                dispatch_async(dispatch_get_main_queue(), {() in
                    SVProgressHUD.dismiss()
                    response(nil, error)
                })
            }
        })
        task.resume()
    }
}