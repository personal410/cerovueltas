//
//  LoadingImageView.swift
//  Sabores
//
//  Created by victor salazar on 28/09/15.
//  Copyright © 2015 Victor Salazar. All rights reserved.
//
import UIKit
class LoadingImageView:CustomImageView{
    let actityIndicator = UIActivityIndicatorView(frame: CGRectZero)
    var strUrl:String? {
        didSet{
            if strUrl != nil {
                let url = NSURL(string: strUrl!)!
                let filename = url.lastPathComponent!
                let imageDirPath = ToolBox.getImagesDirectory()
                let imagePath = "\(imageDirPath)/\(filename)"
                if NSFileManager.defaultManager().fileExistsAtPath(imagePath) {
                    self.image = UIImage(contentsOfFile: imagePath)
                    self.backgroundColor = UIColor.clearColor()
                    self.actityIndicator.stopAnimating()
                    self.actityIndicator.hidden = true
                }else{
                    self.backgroundColor = UIColor.lightGrayColor()
                    self.actityIndicator.hidden = false
                    actityIndicator.startAnimating()
                    let imageUrl = strUrl!
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), {
                        if let imageData = NSData(contentsOfURL: NSURL(string: imageUrl)!) {
                            if !NSFileManager.defaultManager().fileExistsAtPath(imagePath) {
                                imageData.writeToFile(imagePath, atomically: false)
                            }
                            dispatch_async(dispatch_get_main_queue(), {
                                let url2 = NSURL(string: self.strUrl!)!
                                let filename2 = url2.lastPathComponent!
                                if filename2 == filename {
                                    self.actityIndicator.stopAnimating()
                                    self.actityIndicator.hidden = true
                                    self.backgroundColor = UIColor.clearColor()
                                    self.image = UIImage(data: imageData)
                                }
                            })
                        }else{
                            self.actityIndicator.stopAnimating()
                            self.actityIndicator.hidden = true
                        }
                    })
                }
            }else{
                image = nil
            }
        }
    }
    required init?(coder aDecoder:NSCoder){
        super.init(coder:aDecoder)
        self.actityIndicator.activityIndicatorViewStyle = self.frame.size.width < 40 && self.frame.size.height < 40 ? .White : .WhiteLarge
        self.addSubview(self.actityIndicator)
    }
    override init(frame: CGRect){
        super.init(frame: frame)
    }
    override func layoutSubviews(){
        super.layoutSubviews()
        self.actityIndicator.frame = bounds
    }
}