//
//  Color.swift
//  CeroVueltas
//
//  Created by victor salazar on 31/08/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import Foundation
import CoreData
class Color:NSManagedObject{
    class func getColorWithDic(dicColor:Dictionary<String, AnyObject>) -> Color {
        let ctxt = AppDelegate.getMoc()
        let fetchReq = NSFetchRequest(entityName: "Color")
        let tempId = (dicColor["id"] as! NSString).integerValue
        fetchReq.predicate = NSPredicate(format: "id = %i", tempId)
        let results = try! ctxt.executeFetchRequest(fetchReq) as! [Color]
        let color:Color!
        if results.count > 0 {
            color = results.first!
        }else{
            color = NSEntityDescription.insertNewObjectForEntityForName("Color", inManagedObjectContext: ctxt) as! Color
            color.id = tempId
        }
        color.color = dicColor["color"] as? String
        color.name = dicColor["name"] as? String
        color.productId = (dicColor["product_id"] as! NSString).integerValue
        try! ctxt.save()
        return color
    }
}