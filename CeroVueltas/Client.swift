//
//  Client.swift
//  CeroVueltas
//
//  Created by victor salazar on 31/08/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import Foundation
import CoreData
class Client:NSManagedObject{
    class func getClient() -> Client? {
        let ctxt = AppDelegate.getMoc()
        let fetchReq = NSFetchRequest(entityName: "Client")
        do{
            let results = try ctxt.executeFetchRequest(fetchReq) as! [Client]
            if results.count == 0 {
                return nil
            }else{
                return results.first
            }
        }catch let error as NSError {
            print("getClient error: \(error.description)")
            return nil
        }
    }
    class func saveClient(dicClient:Dictionary<String, AnyObject>){
        let ctxt = AppDelegate.getMoc()
        let newClient = NSEntityDescription.insertNewObjectForEntityForName("Client", inManagedObjectContext: ctxt) as! Client
        let tempId = dicClient["id"]
        if let strId = tempId as? NSString {
            newClient.id = strId.integerValue
        }else if let numId = tempId as? NSNumber {
            newClient.id = numId
        }
        newClient.name  = dicClient["name"] as? String
        newClient.email  = dicClient["email"] as? String
        newClient.photo  = dicClient["photo"] as? String
        newClient.phone  = dicClient["phone"] as? String
        newClient.birthday  = dicClient["birthday"] as? String
        newClient.fbId  = dicClient["fb_id"] as? String
        newClient.stores  = dicClient["stores"] as? String
        if newClient.stores == nil {
            newClient.stores = ""
        }
        newClient.products  = dicClient["products"] as? String
        if newClient.products == nil {
            newClient.products = ""
        }
        do{
            try ctxt.save()
        }catch let error as NSError {
            print("saveClient error: \(error.description)")
        }
    }
    func isFollowingStore(storeId:Int) -> Bool {
        let arrStoreIds = self.stores!.componentsSeparatedByString(",").map(){($0 as NSString).integerValue}
        return arrStoreIds.indexOf(storeId) != nil
    }
    func followStore(storeId:Int){
        if stores!.characters.count == 0 {
            stores = "\(storeId)"
        }else{
            stores = "\(stores!),\(storeId)"
        }
        let ctxt = AppDelegate.getMoc()
        do{
            try ctxt.save()
        }catch let error as NSError {
            print("saveClient error: \(error.description)")
        }
    }
    func stopFollowingStore(storeId:Int){
        var arrStoreIds = self.stores!.componentsSeparatedByString(",").map(){($0 as NSString).integerValue}
        arrStoreIds.removeAtIndex(arrStoreIds.indexOf(storeId)!)
        self.stores = arrStoreIds.map(){"\($0)"}.joinWithSeparator(",")
        let ctxt = AppDelegate.getMoc()
        do{
            try ctxt.save()
        }catch let error as NSError {
            print("saveClient error: \(error.description)")
        }
    }
    func isFavoriteProduct(productId:Int) -> Bool {
        let arrProductIds = self.products!.componentsSeparatedByString(",").map(){($0 as NSString).integerValue}
        return arrProductIds.indexOf(productId) != nil
    }
    func productFavorite(productId:Int, isFavorite favorite:Bool){
        var arrProductIds = self.products!.componentsSeparatedByString(",").map(){($0 as NSString).integerValue}
        if favorite {
            arrProductIds.append(productId)
        }else{
            if let index = arrProductIds.indexOf(productId) {
                arrProductIds.removeAtIndex(index)
            }
        }
        self.products = arrProductIds.map(){"\($0)"}.joinWithSeparator(",")
        let ctxt = AppDelegate.getMoc()
        do{
            try ctxt.save()
        }catch let error as NSError {
            print("saveClient error: \(error.description)")
        }
    }
    class func updateClient(){
        let ctxt = AppDelegate.getMoc()
        do{
            try ctxt.save()
        }catch let error as NSError {
            print("saveClient error: \(error.description)")
        }
    }
    class func deleteClient(){
        let ctxt = AppDelegate.getMoc()
        ctxt.deleteObject(Client.getClient()!)
        do{
            try ctxt.save()
        }catch let error as NSError {
            print("saveClient error: \(error.description)")
        }
    }
}
