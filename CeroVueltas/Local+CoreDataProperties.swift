//
//  Local+CoreDataProperties.swift
//  CeroVueltas
//
//  Created by victor salazar on 31/08/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//
import Foundation
import CoreData
extension Local{
    @NSManaged var address:String?
    @NSManaged var id:NSNumber
    @NSManaged var latitude:Double
    @NSManaged var longitude:Double
    @NSManaged var name:String?
    @NSManaged var schedule:String?
    @NSManaged var storeId:NSNumber
    @NSManaged var store:Store?
    @NSManaged var products:NSSet?
}
