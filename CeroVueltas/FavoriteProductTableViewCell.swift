//
//  FavoriteProductTableViewCell.swift
//  CeroVueltas
//
//  Created by victor salazar on 29/08/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class FavoriteProductTableViewCell:UITableViewCell{
    @IBOutlet weak var productImgView:LoadingImageView!
    @IBOutlet weak var productNameLbl:UILabel!
    @IBOutlet weak var productBrandLbl:UILabel!
    @IBOutlet weak var storeNameLbl:UILabel!
    @IBOutlet weak var likeBtn:UIButton!
    @IBOutlet weak var bottomLineView:UIView!
}