//
//  Store.swift
//  CeroVueltas
//
//  Created by victor salazar on 31/08/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import Foundation
import CoreData
class Store:NSManagedObject{
    var clientFollowing:Bool! = false
    class func getStoreWithDic(dicStore:Dictionary<String, AnyObject>) -> Store {
        //print("dicStore: \(dicStore)")
        let ctxt = AppDelegate.getMoc()
        let fetchReq = NSFetchRequest(entityName: "Store")
        let tempId = (dicStore["id"] as! NSString).integerValue
        fetchReq.predicate = NSPredicate(format: "id = %i", tempId)
        let results = try! ctxt.executeFetchRequest(fetchReq) as! [Store]
        let storeTemp:Store!
        if results.count > 0 {
            storeTemp = results.first!
        }else{
            storeTemp = NSEntityDescription.insertNewObjectForEntityForName("Store", inManagedObjectContext: ctxt) as! Store
            storeTemp.id = tempId
        }
        storeTemp.storeDescription = dicStore["description"] as? String
        storeTemp.shipments = dicStore["shipments"] as? String
        storeTemp.email = dicStore["email"] as? String
        storeTemp.followers = dicStore["followers"] as! Int
        storeTemp.name = dicStore["name"] as? String
        storeTemp.numberLocals = dicStore["number_of_locals"] as! Int
        storeTemp.numberProducts = dicStore["number_of_products"] as! Int
        storeTemp.phone = dicStore["phone"] as? String
        storeTemp.contactPhone = dicStore["phone_contact"] as? String
        storeTemp.photoURL = dicStore["photo_url"] as? String
        storeTemp.slogan = dicStore["slogan"] as? String
        storeTemp.userId = (dicStore["user_id"] as! NSString).integerValue
        storeTemp.web = dicStore["web"] as? String
        storeTemp.facebookURL = dicStore["facebook"] as? String
        storeTemp.instagramURL = dicStore["instagram"] as? String
        storeTemp.listProducts = dicStore["products"] as? String
        if let currentClient = Client.getClient() {
            storeTemp.clientFollowing = currentClient.isFollowingStore(tempId)
        }
        var arrLocals:Array<Local> = []
        if let arrDicLocals = dicStore["locals"] as? Array<Dictionary<String, AnyObject>> {
            for dicLocal in arrDicLocals {
                arrLocals.append(Local.getLocalWithDic(dicLocal))
            }
        }
        storeTemp.locals = NSOrderedSet(array: arrLocals)
        if let productsByStore = dicStore["products_by_store"] as? Array<Dictionary<String, AnyObject>> {
            for dicProduct in productsByStore {
                Product.getProductWithDic(dicProduct, store: storeTemp)
            }
        }
        try! ctxt.save()
        return storeTemp
    }
    class func transformDicStores(arrDicStores:Array<Dictionary<String, AnyObject>>) -> Array<Store> {
        var arrStore:Array<Store> = []
        for dicStore in arrDicStores {
            arrStore.append(Store.getStoreWithDic(dicStore))
        }
        return arrStore
    }
    class func getStoresWithStoreIds(arrStoreIds:Array<Int>) -> Array<Store> {
        let ctxt = AppDelegate.getMoc()
        let fetchReq = NSFetchRequest(entityName: "Store")
        fetchReq.predicate = NSPredicate(format: "id in %@", arrStoreIds)
        let results = try! ctxt.executeFetchRequest(fetchReq) as! Array<Store>
        return results
    }
}
