//
//  SignUpViewController.swift
//  CeroVueltas
//
//  Created by victor salazar on 1/08/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class SignUpViewController:UIViewController, UITextFieldDelegate{
    @IBOutlet var fullNameTxtFld:UITextField!
    @IBOutlet var birthdayTxtFld:UITextField!
    @IBOutlet var emailTxtFld:UITextField!
    @IBOutlet var passwordTxtFld:UITextField!
    @IBOutlet var signUpView:UIView!
    override func viewDidLoad(){
        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(SignUpViewController.showKeyboard(_:)), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(SignUpViewController.hideKeyboard(_:)), name: UIKeyboardWillHideNotification, object: nil)
        let datePicker = UIDatePicker()
        datePicker.locale = NSLocale(localeIdentifier: "es_ES")
        datePicker.datePickerMode = .Date
        datePicker.addTarget(self, action: #selector(SignUpViewController.datePickerDidChange(_:)), forControlEvents: .ValueChanged)
        self.birthdayTxtFld.inputView = datePicker
        let flexSpaceBarBtnItem = UIBarButtonItem(barButtonSystemItem: .FlexibleSpace, target: nil, action: nil)
        let okBarBtnItem = UIBarButtonItem(title: "OK", style: .Done, target: self, action: #selector(SignUpViewController.confirmDate))
        let toolbar = UIToolbar()
        toolbar.backgroundColor = UIColor.blackColor()
        toolbar.items = [flexSpaceBarBtnItem, okBarBtnItem]
        toolbar.sizeToFit()
        self.birthdayTxtFld.inputAccessoryView = toolbar
    }
    override func viewDidDisappear(animated:Bool){
        super.viewDidDisappear(animated)
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    //MARK: - IBAction
    @IBAction func close(){
        dismissViewControllerAnimated(true, completion: nil)
    }
    @IBAction func dissmisKeyboard(){
        if !self.fullNameTxtFld.resignFirstResponder() {
            if !self.birthdayTxtFld.resignFirstResponder() {
                if !self.emailTxtFld.resignFirstResponder() {
                    self.passwordTxtFld.resignFirstResponder()
                }
            }
        }
    }
    @IBAction func signUp(){
        self.dissmisKeyboard()
        if self.fullNameTxtFld.text!.isEmpty || self.emailTxtFld.text!.isEmpty || self.passwordTxtFld.text!.isEmpty {
            ToolBox.showAlertWithTitle("Alerta", withMessage: "Todos los campos son requeridos.", inViewCont: self)
        }else{
            SVProgressHUD.showWithStatus("Registrando")
            let dicParams = ["name": self.fullNameTxtFld.text!, "birthday": self.birthdayTxtFld.text!, "email" : self.emailTxtFld.text!, "password" : self.passwordTxtFld.text!]
            ServiceConnector.connectToUrl(URLs.signUpURL, params: ToolBox.convertDictionaryToPostParams(dicParams), response: { (result:AnyObject?, error:NSError?) in
                if error == nil {
                    if let dicResult = result as? Dictionary<String, AnyObject> {
                        if let state = dicResult["state"] as? String {
                            if state == "success" {
                                let dicClient = dicResult["user"] as! Dictionary<String, AnyObject>
                                Client.saveClient(dicClient)
                                self.performSegueWithIdentifier("showUserTabBar", sender: nil)
                                return
                            }else if state == "error" {
                                if let message = dicResult["message"] as? String {
                                    ToolBox.showAlertWithTitle("Alerta", withMessage: message, inViewCont: self)
                                    return
                                }
                            }
                        }
                    }
                    ToolBox.showErrorConnectionInViewCont(self)
                }else{
                    ToolBox.showErrorConnectionInViewCont(self)
                }
            })
        }
    }
    func datePickerDidChange(datePicker:UIDatePicker){
        let dateFormat = NSDateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd"
        self.birthdayTxtFld.text = dateFormat.stringFromDate(datePicker.date)
    }
    func confirmDate(){
        self.birthdayTxtFld.resignFirstResponder()
        let datePicker = self.birthdayTxtFld.inputView as! UIDatePicker
        datePickerDidChange(datePicker)
    }
    //MARK: - Keyboard
    func showKeyboard(notification:NSNotification){
        var info = notification.userInfo!
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as! NSValue).CGRectValue()
        let keyboardHeight = keyboardSize.height
        UIView.animateWithDuration((info[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue, animations: {
            self.signUpView.transform = CGAffineTransformMakeTranslation(0, -keyboardHeight);
        })
    }
    func hideKeyboard(notification:NSNotification){
        var info = notification.userInfo!
        UIView.animateWithDuration((info[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue, animations: {
            self.signUpView.transform = CGAffineTransformIdentity;
        })
    }
    //MARK: - TextField
    func textFieldShouldReturn(textField:UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
