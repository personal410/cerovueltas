//
//  URLs.swift
//  CeroVueltas
//
//  Created by victor salazar on 6/08/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import Foundation
class URLs{
    static let rootURL = "http://cerovueltas.com/ws/"
    static let key = "73716dd585ca8513915414111f7a1f16142b662f"
    static let baseURL = "\(rootURL)\(key)/"
    static let loginURL = "\(baseURL)log-in"
    static let signUpURL = "\(baseURL)sign-up"
    static let getProductsURL = "\(baseURL)get-products"
    static let getNewsURL = "\(baseURL)get-products-by-follows"
    static let getFilterProductsURL = "\(baseURL)get-products-by-filters"
    static let getDirectoryURL = "\(baseURL)get-directory"
    static let followStoreURL = "\(baseURL)set-follow"
    static let getFavoritesURL = "\(baseURL)get-favorites"
    static let productFavoriteURL = "\(baseURL)set-favorite"
    static let updateClientURL = "\(baseURL)update/"
    static let fbLoginURL = "\(baseURL)fb-log-in"
    static let recoverPasswordURL = "\(baseURL)forget-password"
}
