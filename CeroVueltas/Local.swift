//
//  Local.swift
//  CeroVueltas
//
//  Created by victor salazar on 31/08/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import Foundation
import CoreData
class Local:NSManagedObject{
    class func getLocalWithDic(dicLocal:Dictionary<String, AnyObject>) -> Local {
        let ctxt = AppDelegate.getMoc()
        let fetchReq = NSFetchRequest(entityName: "Local")
        let tempId = (dicLocal["id"] as! NSString).integerValue
        fetchReq.predicate = NSPredicate(format: "id = %i", tempId)
        let results = try! ctxt.executeFetchRequest(fetchReq) as! [Local]
        let local:Local!
        if results.count > 0 {
            local = results.first!
        }else{
            local = NSEntityDescription.insertNewObjectForEntityForName("Local", inManagedObjectContext: ctxt) as! Local
            local.id = tempId
        }
        local.address = dicLocal["address"] as? String
        local.latitude = (dicLocal["latitude"] as! NSString).doubleValue
        local.longitude = (dicLocal["longitude"] as! NSString).doubleValue
        local.name = dicLocal["name"] as? String
        local.schedule = dicLocal["schedule"] as? String
        local.storeId = (dicLocal["store_id"] as! NSString).integerValue
        try! ctxt.save()
        return local
    }
    class func getLocalWithLocalId(localId:Int) -> Local? {
        let ctxt = AppDelegate.getMoc()
        let fetchReq = NSFetchRequest(entityName: "Local")
        fetchReq.predicate = NSPredicate(format: "id = %i", localId)
        do{
            let arrLocal = try ctxt.executeFetchRequest(fetchReq)
            if arrLocal.count > 0 {
                return arrLocal.first as? Local
            }else{
                return nil
            }
        }catch{
            return nil
        }
    }
}
