//
//  Photo+CoreDataProperties.swift
//  CeroVueltas
//
//  Created by victor salazar on 31/08/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//
import Foundation
import CoreData
extension Photo{
    @NSManaged var id:NSNumber
    @NSManaged var photo:String?
    @NSManaged var photoURL:String?
    @NSManaged var principal:NSNumber
    @NSManaged var productId:NSNumber
    @NSManaged var products:NSSet?
}