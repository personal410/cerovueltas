//
//  Product.swift
//  CeroVueltas
//
//  Created by victor salazar on 31/08/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import Foundation
import CoreData
class Product:NSManagedObject{
    var isFavorite:Bool! = false
    class func getProductWithDic(dicProduct:Dictionary<String, AnyObject>, store:Store? = nil) -> Product {
        let ctxt = AppDelegate.getMoc()
        let fetchReq = NSFetchRequest(entityName: "Product")
        let tempId = (dicProduct["id"] as! NSString).integerValue
        fetchReq.predicate = NSPredicate(format: "id = %i", tempId)
        let results = try! ctxt.executeFetchRequest(fetchReq) as! [Product]
        let productTemp:Product!
        if results.count > 0 {
            productTemp = results.first!
        }else{
            productTemp = NSEntityDescription.insertNewObjectForEntityForName("Product", inManagedObjectContext: ctxt) as! Product
            productTemp.id = tempId
        }
        productTemp.age = dicProduct["age"] as? String
        productTemp.brand = dicProduct["brand"] as? String
        if let dicCategory = dicProduct["category"] as? Dictionary<String, AnyObject> {
            productTemp.category = Category.getCategoryWithDic(dicCategory)
        }
        productTemp.categoryId = (dicProduct["category_id"] as! NSString).integerValue
        productTemp.characteristics = dicProduct["characteristics"] as? String
        var arrColors:Array<Color> = []
        if let arrDicColors = dicProduct["colors"] as? Array<Dictionary<String, AnyObject>> {
            for dicColor in arrDicColors {
                arrColors.append(Color.getColorWithDic(dicColor))
            }
        }
        productTemp.colors = NSOrderedSet(array: arrColors)
        productTemp.productDescription = dicProduct["description"] as? String
        productTemp.gender = dicProduct["gender"] as? String
        productTemp.name = dicProduct["name"] as? String
        productTemp.photoURL = dicProduct["photo_url"] as? String
        var arrPhotos:Array<Photo> = []
        if let arrDicPhotos = dicProduct["photos"] as? Array<Dictionary<String, AnyObject>> {
            for dicPhoto in arrDicPhotos {
                arrPhotos.append(Photo.getPhotoWithDic(dicPhoto))
            }
        }
        productTemp.photos = NSOrderedSet(array: arrPhotos)
        productTemp.price = (dicProduct["price"] as! NSString).doubleValue
        var arrSizes:Array<Size> = []
        if let arrDicSizes = dicProduct["sizes"] as? Array<Dictionary<String, AnyObject>> {
            for dicSize in arrDicSizes {
                arrSizes.append(Size.getSizeWithDic(dicSize))
            }
        }
        productTemp.sizes = NSOrderedSet(array: arrSizes)
        if let dicStore = dicProduct["store"] as? Dictionary<String, AnyObject> {
            productTemp.store = Store.getStoreWithDic(dicStore)
        }else{
            productTemp.store = store
        }
        productTemp.storeId = (dicProduct["store_id"] as? NSString)!.integerValue
        productTemp.tags = dicProduct["tags"] as? String
        if let client = Client.getClient() {
            productTemp.isFavorite = client.isFavoriteProduct(tempId)
        }
        var arrLocals:Array<Local> = []
        if let arrDicLocals = dicProduct["product_by_locals"] as? Array<Dictionary<String, AnyObject>> {
            for dicLocal in arrDicLocals {
                if let localId = dicLocal["store_local_id"] as? NSString {
                    let localId2 = localId.integerValue
                    if let local = Local.getLocalWithLocalId(localId2) {
                        arrLocals.append(local)
                    }
                }else if let localId2 = dicLocal["store_local_id"] as? Int {
                    if let local = Local.getLocalWithLocalId(localId2) {
                        arrLocals.append(local)
                    }
                }
            }
        }
        productTemp.locals = NSOrderedSet(array: arrLocals)
        do{
            try ctxt.save()
        }catch let error as NSError {
            print("getProductWithDic error: \(error)")
        }
        return productTemp
    }
    class func transformDicProducts(arrDicProducts:Array<Dictionary<String, AnyObject>>) -> Array<Product> {
        var arrProducts:Array<Product> = []
        for dicProduct in arrDicProducts {
            arrProducts.append(Product.getProductWithDic(dicProduct))
        }
        return arrProducts
    }
    func getPriceRange() -> Int {
        if self.price.doubleValue <= 30 {
            return 1
        }else if self.price.doubleValue <= 100 {
            return 2
        }else if self.price.doubleValue <= 200 {
            return 3
        }else if self.price.doubleValue <= 300 {
            return 4
        }else{
            return 5
        }
    }
    class func getProductsWithProductIds(arrProductIds:Array<Int>) -> Array<Product> {
        let ctxt = AppDelegate.getMoc()
        let fetchReq = NSFetchRequest(entityName: "Product")
        fetchReq.predicate = NSPredicate(format: "id in %@", arrProductIds)
        let results = try! ctxt.executeFetchRequest(fetchReq) as! Array<Product>
        return results
    }
    class func deleteProducts(){
        let ctxt = AppDelegate.getMoc()
        let fetchReq = NSFetchRequest(entityName: "Product")
        let results = try! ctxt.executeFetchRequest(fetchReq) as! Array<Product>
        for product in results {
            ctxt.deleteObject(product)
        }
        try! ctxt.save()
    }
    class func getProductsWithSearchText(searchText:String) -> Array<Product> {
        let ctxt = AppDelegate.getMoc()
        let fetchReq = NSFetchRequest(entityName: "Product")
        fetchReq.predicate = NSPredicate(format: "tags contains[c] %@", searchText)
        let results = try! ctxt.executeFetchRequest(fetchReq) as! Array<Product>
        return results
    }
}
