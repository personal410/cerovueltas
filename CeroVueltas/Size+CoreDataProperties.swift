//
//  Size+CoreDataProperties.swift
//  CeroVueltas
//
//  Created by victor salazar on 31/08/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//
import Foundation
import CoreData
extension Size{
    @NSManaged var id:NSNumber
    @NSManaged var name:String?
    @NSManaged var productId:NSNumber
    @NSManaged var size:String?
    @NSManaged var stock:NSNumber
    @NSManaged var products:NSSet?
}