//
//  FilterOption.swift
//  CeroVueltas
//
//  Created by victor salazar on 10/08/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import Foundation
class FilterOption{
    var code:String!
    var title:String!
    init(nCode:String, nTitle:String){
        code = nCode
        title = nTitle
    }
}