//
//  ColorProductDetailCollectionViewCell.swift
//  CeroVueltas
//
//  Created by victor salazar on 24/08/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class ColorProductDetailCollectionViewCell:UICollectionViewCell{
    @IBOutlet weak var colorView:UIView!
    @IBOutlet weak var colorNameLbl:UILabel!
}