//
//  Size.swift
//  CeroVueltas
//
//  Created by victor salazar on 31/08/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import Foundation
import CoreData
class Size:NSManagedObject{
    class func getSizeWithDic(dicSize:Dictionary<String, AnyObject>) -> Size {
        let ctxt = AppDelegate.getMoc()
        let fetchReq = NSFetchRequest(entityName: "Size")
        let tempId = (dicSize["id"] as! NSString).integerValue
        fetchReq.predicate = NSPredicate(format: "id = %i", tempId)
        let results = try! ctxt.executeFetchRequest(fetchReq) as! [Size]
        let size:Size!
        if results.count > 0 {
            size = results.first!
        }else{
            size = NSEntityDescription.insertNewObjectForEntityForName("Size", inManagedObjectContext: ctxt) as! Size
            size.id = tempId
        }
        size.name = dicSize["name"] as? String
        size.productId = (dicSize["product_id"] as! NSString).integerValue
        size.size = dicSize["size"] as? String
        if let stockTemp = dicSize["stock"] as? Int {
            size.stock = stockTemp
        }else{
            size.stock = 0
        }
        try! ctxt.save()
        return size
    }
}