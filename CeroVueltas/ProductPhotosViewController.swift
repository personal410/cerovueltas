//
//  ProductsPhotosViewController.swift
//  CeroVueltas
//
//  Created by victor salazar on 15/09/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class ProductPhotosViewController:UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    //MARK: - Variables
    var arrPhotos:Array<Photo>!
    //MARK: - IBOutlet
    @IBOutlet weak var closeBtn:UIButton!
    @IBOutlet weak var photosPageControl:UIPageControl!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        closeBtn.setImage(closeBtn.currentImage?.imageWithRenderingMode(.AlwaysTemplate), forState: .Normal)
        photosPageControl.numberOfPages = arrPhotos.count
    }
    //MARK: - IBAction
    @IBAction func close(){
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    //MARK: - CollectionView
    func collectionView(collectionView:UICollectionView, numberOfItemsInSection section:Int) -> Int {
        return arrPhotos.count
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: view.frame.width)
    }
    func collectionView(collectionView:UICollectionView, cellForItemAtIndexPath indexPath:NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("photoCell", forIndexPath: indexPath) as! ProductPhotoCollectionViewCell
        cell.productPhotoImgView.strUrl = arrPhotos[indexPath.item].photo
        return cell
    }
    func scrollViewDidEndDecelerating(scrollView:UIScrollView){
        photosPageControl.currentPage = Int(round(scrollView.contentOffset.x / view.frame.width))
    }
}
