//
//  StoreViewController.swift
//  CeroVueltas
//
//  Created by victor salazar on 4/09/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
import MessageUI
class StoreViewController:UIViewController, UITableViewDataSource, UITableViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITextFieldDelegate, MFMailComposeViewControllerDelegate {
    //MARK: - Variables
    var store:Store!
    var selectedOption:Int! = 0
    var selectedBtn:UIButton!
    var arrLocals:Array<Local> = []
    //MARK: - IBOutlet
    @IBOutlet weak var storeImgView:LoadingImageView!
    @IBOutlet weak var storeNameLbl:UILabel!
    @IBOutlet weak var sloganNameLbl:UILabel!
    @IBOutlet weak var numberProductsLbl:UILabel!
    @IBOutlet weak var numberFollowersLbl:UILabel!
    @IBOutlet weak var followBtn:UIButton!
    @IBOutlet weak var descriptionView:UIView!
    @IBOutlet weak var descriptionBtn:UIButton!
    @IBOutlet weak var photosBtn:UIButton!
    @IBOutlet weak var descriptionLbl:UILabel!
    @IBOutlet weak var productsLbl:UILabel!
    @IBOutlet weak var shipmentHeightCons:NSLayoutConstraint!
    @IBOutlet weak var shipmentsLbl:UILabel!
    @IBOutlet weak var emailHeightCons:NSLayoutConstraint!
    @IBOutlet weak var emailLbl:UILabel!
    @IBOutlet weak var phoneHeightCons:NSLayoutConstraint!
    @IBOutlet weak var phoneLbl:UILabel!
    @IBOutlet weak var contactNumberHeightCons:NSLayoutConstraint!
    @IBOutlet weak var contactNumberLbl:UILabel!
    @IBOutlet weak var webHeightCons:NSLayoutConstraint!
    @IBOutlet weak var webLbl:UILabel!
    @IBOutlet weak var fbHeightCons:NSLayoutConstraint!
    @IBOutlet weak var instagramHeightCons:NSLayoutConstraint!
    @IBOutlet weak var localsTableView:UITableView!
    @IBOutlet weak var heightLocalsTableViewConstraint:NSLayoutConstraint!
    @IBOutlet weak var storePhotosCollectionView:UICollectionView!
    @IBOutlet weak var heightStorePhotosCollectionViewConstraint:NSLayoutConstraint!
    @IBOutlet weak var bottomSpaceStorePhotosCollectionViewConstraint:NSLayoutConstraint!
    @IBOutlet weak var searchTextFld:UITextField!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        self.storeImgView.strUrl = self.store.photoURL
        self.storeNameLbl.text = self.store.name
        self.sloganNameLbl.text = self.store.slogan
        self.numberProductsLbl.text = "\(self.store.numberProducts.integerValue)"
        self.numberFollowersLbl.text = "\(self.store.followers.integerValue)"
        self.followBtn.setTitle(self.store.clientFollowing! ? "Seguido" : "Seguir", forState: .Normal)
        self.descriptionLbl.text = self.store.storeDescription
        self.productsLbl.text = self.store.listProducts
        if let shipment = store.shipments {
            if !shipment.isEmpty {
                shipmentHeightCons.priority = 100
                shipmentsLbl.text = store.shipments
            }
        }
        if let email = self.store.email {
            if email.characters.count > 0 {
                self.emailLbl.text = email
            }else{
                self.emailHeightCons.constant = 0
            }
        }else{
            self.emailHeightCons.constant = 0
        }
        if let phone = self.store.phone {
            if phone.characters.count > 0 {
                self.phoneLbl.text = phone
            }else{
                self.phoneHeightCons.constant = 0
            }
        }else{
            self.phoneHeightCons.constant = 0
        }
        if let contactPhone = self.store.contactPhone {
            if contactPhone.characters.count > 0 {
                self.contactNumberLbl.text = contactPhone
            }else{
                self.contactNumberHeightCons.constant = 0
            }
        }else{
            self.contactNumberHeightCons.constant = 0
        }
        if let web = self.store.web {
            if web.characters.count > 0 {
                self.webLbl.text = web
            }else{
                self.webHeightCons.constant = 0
            }
        }else{
            self.webHeightCons.constant = 0
        }
        if let facebookURL = self.store.facebookURL {
            if facebookURL.characters.count == 0 {
                self.fbHeightCons.constant = 0
            }
        }else{
            self.fbHeightCons.constant = 0
        }
        if let instagramURL = self.store.instagramURL {
            if instagramURL.characters.count == 0 {
                self.instagramHeightCons.constant = 0
            }
        }else{
            self.instagramHeightCons.constant = 0
        }
        self.arrLocals = self.store.locals!.array as! Array<Local>
        self.heightLocalsTableViewConstraint.constant = CGFloat(self.arrLocals.count) * 23
        self.selectedBtn = self.descriptionBtn
        let numberRows = ceil(CGFloat(self.store.products!.array.count) / 3.0)
        self.heightStorePhotosCollectionViewConstraint.constant = numberRows * 80 + (numberRows > 0 ? (numberRows - 1) * 10 : 0)
    }
    override func prepareForSegue(segue:UIStoryboardSegue, sender:AnyObject?){
        if let viewCont = segue.destinationViewController as? MapLocalViewController {
            viewCont.local = self.arrLocals[(sender as! UIButton).tag]
        }else if let viewCont = segue.destinationViewController as? ProductDetailViewController {
            viewCont.product = self.store.products!.array[self.storePhotosCollectionView.indexPathsForSelectedItems()!.first!.item] as! Product
        }
    }
    //MARK: - IBAction
    @IBAction func back(){
        self.navigationController?.popViewControllerAnimated(true)
    }
    @IBAction func selectOption(btn:UIButton){
        if self.selectedOption == btn.tag {
            return
        }else{
            let previousSelected = self.selectedOption
            self.selectedBtn.setTitleColor(UIColor.darkGrayColor(), forState: .Normal)
            self.selectedBtn = btn
            self.selectedOption = btn.tag
            self.selectedBtn.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            let width = self.view.frame.width
            let duration = 0.25
            if previousSelected == 0 {
                self.storePhotosCollectionView.hidden = false
                self.storePhotosCollectionView.transform = CGAffineTransformMakeTranslation(width, 0)
                self.bottomSpaceStorePhotosCollectionViewConstraint.priority = 200
                UIView.animateWithDuration(duration, delay: 0, options: .CurveEaseInOut, animations: {
                    self.storePhotosCollectionView.transform = CGAffineTransformIdentity
                    self.descriptionView.transform = CGAffineTransformMakeTranslation(-width, 0)
                    self.view.layoutIfNeeded()
                    }, completion: {(b:Bool) in
                        self.descriptionView.transform = CGAffineTransformIdentity
                        self.descriptionView.hidden = true
                })
            }else{
                self.descriptionView.hidden = false
                self.descriptionView.transform = CGAffineTransformMakeTranslation(-width, 0)
                self.bottomSpaceStorePhotosCollectionViewConstraint.priority = 100
                UIView.animateWithDuration(duration, delay: 0, options: .CurveEaseInOut, animations: {
                    self.descriptionView.transform = CGAffineTransformIdentity
                    self.storePhotosCollectionView.transform = CGAffineTransformMakeTranslation(width, 0)
                    self.view.layoutIfNeeded()
                    }, completion: {(b:Bool) in
                        self.storePhotosCollectionView.transform = CGAffineTransformIdentity
                        self.storePhotosCollectionView.hidden = true
                })
            }
        }
    }
    @IBAction func follow(){
        if Client.getClient() == nil {
            ToolBox.showNoAccountMessageInViewCont(self.navigationController!.tabBarController!)
        }else{
            let url = "\(URLs.followStoreURL)/\(self.store.id)/\(Client.getClient()!.id)"
            self.store.clientFollowing = !self.store.clientFollowing
            self.store.followers = self.store.clientFollowing! ? (self.store.followers.integerValue + 1) : (self.store.followers.integerValue - 1)
            self.followBtn.setTitle(self.store.clientFollowing! ? "Seguido" : "Seguir", forState: .Normal)
            self.numberFollowersLbl.text = "\(self.store.followers.integerValue)"
            ServiceConnector.connectToUrl(url, method: "GET") { (result:AnyObject?, error:NSError?) in
                if error == nil {
                    if let dicResult = result as? Dictionary<String, AnyObject> {
                        if let state = dicResult["state"] as? String {
                            if state == "success" {
                                if self.store.clientFollowing! {
                                    Client.getClient()?.followStore(Int(self.store.id))
                                }else{
                                    Client.getClient()?.stopFollowingStore(Int(self.store.id))
                                }
                                return
                            }
                        }
                    }
                }
                self.store.clientFollowing = !self.store.clientFollowing
                self.store.followers = self.store.clientFollowing! ? (self.store.followers.integerValue + 1) : (self.store.followers.integerValue - 1)
                self.followBtn.setTitle(self.store.clientFollowing! ? "Seguido" : "Seguir", forState: .Normal)
                self.numberFollowersLbl.text = "\(self.store.followers.integerValue)"
                ToolBox.showAlertWithTitle("Alerta", withMessage: "No se pudo seguir a la tienda.", inViewCont: self)
            }
        }
    }
    @IBAction func makeCall(button:UIButton){
        if button.tag == 0 {
            if let phone = self.store.phone {
                if let url = NSURL(string:"telprompt:\(phone)") {
                    if UIApplication.sharedApplication().openURL(url) {
                        return
                    }
                }
            }
        }else if button.tag == 1 {
            if let phone = self.store.contactPhone {
                if let url = NSURL(string:"telprompt:\(phone)") {
                    if UIApplication.sharedApplication().openURL(url) {
                        return
                    }
                }
            }
        }
        ToolBox.showAlertWithTitle("Alerta", withMessage: "No se pudo hacer la llamada", inViewCont: self)
    }
    @IBAction func searchProducts(){
        if self.searchTextFld.alpha == 0 {
            UIView.animateWithDuration(0.25, animations: {
                self.searchTextFld.alpha = 1
                }, completion:{(b:Bool) in
                    self.searchTextFld.becomeFirstResponder()
            })
        }else{
            self.searchTextFld.resignFirstResponder()
            UIView.animateWithDuration(0.25, animations: {
                self.searchTextFld.alpha = 0
            })
        }
    }
    @IBAction func showEmail(){
        if MFMailComposeViewController.canSendMail() {
            let mailViewCont = MFMailComposeViewController()
            mailViewCont.mailComposeDelegate = self;
            mailViewCont.setToRecipients([store.email!])
            presentViewController(mailViewCont, animated: true, completion: nil)
        }else{
            ToolBox.showAlertWithTitle("Alert", withMessage: "No se puede enviar correos. Revisa tu configuración", inViewCont: self)
        }
    }
    @IBAction func showWeb(){
        if var web = self.store.web {
            if !(web.containsString("http://") || web.containsString("https://")) {
                web = "http://\(web)"
            }
            if let url = NSURL(string: web) {
                if UIApplication.sharedApplication().canOpenURL(url) {
                    UIApplication.sharedApplication().openURL(url)
                    return
                }
            }
        }
        ToolBox.showAlertWithTitle("Alerta", withMessage: "No se pudo abrir la pagina web.", inViewCont: self)
    }
    @IBAction func showFB(){
        if var fb = self.store.facebookURL {
            if !(fb.containsString("http://") || fb.containsString("https://")) {
                fb = "http://\(fb)"
            }
            if let url = NSURL(string: fb) {
                if UIApplication.sharedApplication().canOpenURL(url) {
                    UIApplication.sharedApplication().openURL(url)
                    return
                }
            }
        }
        ToolBox.showAlertWithTitle("Alerta", withMessage: "No se pudo abrir la pagina de facebook de la empresa.", inViewCont: self)
    }
    @IBAction func showInstagram(){
        if var instagram = self.store.instagramURL {
            if !(instagram.containsString("http://") || instagram.containsString("https://")) {
                instagram = "http://\(instagram)"
            }
            if let url = NSURL(string: instagram) {
                if UIApplication.sharedApplication().canOpenURL(url) {
                    UIApplication.sharedApplication().openURL(url)
                    return
                }
            }
        }
        ToolBox.showAlertWithTitle("Alerta", withMessage: "No se pudo abrir la pagina de instagram de la empresa.", inViewCont: self)
    }
    //MARK: - TableView
    func tableView(tableView:UITableView, heightForHeaderInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(tableView:UITableView, heightForFooterInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return self.arrLocals.count
    }
    func tableView(tableView:UITableView, cellForRowAtIndexPath indexPath:NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("storeLocalCell", forIndexPath: indexPath) as! StoreLocalTableViewCell
        let local = self.arrLocals[indexPath.row]
        cell.localNameLbl.text = local.name
        cell.mapBtn.tag = indexPath.row
        return cell
    }
    //MARK: - CollectionView
    func collectionView(collectionView:UICollectionView, numberOfItemsInSection section:Int) -> Int {
        return self.store.products!.array.count
    }
    func collectionView(collectionView:UICollectionView, layout collectionViewLayout:UICollectionViewLayout, sizeForItemAtIndexPath indexPath:NSIndexPath) -> CGSize {
        return CGSizeMake(floor(self.view.frame.width / 3), 80)
    }
    func collectionView(collectionView:UICollectionView, cellForItemAtIndexPath indexPath:NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("storePhotoCell", forIndexPath: indexPath) as! StorePhotoCollectionViewCell
        let product = self.store.products!.array[indexPath.row] as! Product
        cell.storePhotoImgView.strUrl = product.photoURL
        return cell
    }
    //MARK: - TextField
    func textFieldShouldReturn(textField:UITextField) -> Bool {
        textField.resignFirstResponder()
        if textField.text!.isEmpty {
            UIView.animateWithDuration(0.25, animations: {
                self.searchTextFld.alpha = 0
            })
        }else{
            self.searchTextFld.alpha = 0
            let viewCont = self.storyboard!.instantiateViewControllerWithIdentifier("productListViewCont") as! ProductListViewController
            viewCont.searchText = textField.text!
            self.navigationController?.pushViewController(viewCont, animated: true)
        }
        return true
    }
    //MARK: - Mail
    func mailComposeController(controller:MFMailComposeViewController, didFinishWithResult result:MFMailComposeResult, error: NSError?){
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
}
