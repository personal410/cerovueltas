//
//  ProductCollectionViewCell.swift
//  CeroVueltas
//
//  Created by victor salazar on 11/08/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class ProductCollectionViewCell:UICollectionViewCell{
    @IBOutlet var storeNameBtn:UIButton!
    @IBOutlet var productNameLbl:UILabel!
    @IBOutlet var productImgView:LoadingImageView!
    @IBOutlet var likeBtn:UIButton!
}
