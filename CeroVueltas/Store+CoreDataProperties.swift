//
//  Store+CoreDataProperties.swift
//  CeroVueltas
//
//  Created by victor salazar on 31/08/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//
import Foundation
import CoreData
extension Store{
    @NSManaged var email:String?
    @NSManaged var followers:NSNumber
    @NSManaged var id:NSNumber
    @NSManaged var name:String?
    @NSManaged var numberLocals:NSNumber
    @NSManaged var numberProducts:NSNumber
    @NSManaged var shipments:String?
    @NSManaged var phone:String?
    @NSManaged var contactPhone:String?
    @NSManaged var photoURL:String?
    @NSManaged var slogan:String?
    @NSManaged var storeDescription:String?
    @NSManaged var listProducts:String?
    @NSManaged var userId:NSNumber
    @NSManaged var web:String?
    @NSManaged var facebookURL:String?
    @NSManaged var instagramURL:String?
    @NSManaged var locals:NSOrderedSet?
    @NSManaged var products:NSOrderedSet?
}
