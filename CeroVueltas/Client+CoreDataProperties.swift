//
//  Client+CoreDataProperties.swift
//  CeroVueltas
//
//  Created by victor salazar on 31/08/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//
import Foundation
import CoreData
extension Client{
    @NSManaged var birthday:String?
    @NSManaged var email:String?
    @NSManaged var fbId:String?
    @NSManaged var id:NSNumber
    @NSManaged var name:String?
    @NSManaged var phone:String?
    @NSManaged var photo:String?
    @NSManaged var products:String?
    @NSManaged var stores:String?
}