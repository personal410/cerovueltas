//
//  FollowingStoreCollectionViewCell.swift
//  CeroVueltas
//
//  Created by victor salazar on 5/09/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class FollowingStoreCollectionViewCell:UICollectionViewCell{
    @IBOutlet weak var storePhotoImgView:LoadingImageView!
    @IBOutlet weak var storeNameLbl:UILabel!
}