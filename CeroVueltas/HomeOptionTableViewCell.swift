//
//  HomeOptionTableViewCell.swift
//  CeroVueltas
//
//  Created by Victor Salazar on 10/08/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class HomeOptionTableViewCell:UITableViewCell{
    @IBOutlet var optionLbl:UILabel!
    @IBOutlet var checkedImgView:UIImageView!
    @IBOutlet var separatorView:UIView!
}