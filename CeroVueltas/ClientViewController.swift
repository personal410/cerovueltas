//
//  ClientViewController.swift
//  CeroVueltas
//
//  Created by victor salazar on 5/09/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class ClientViewController:UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITextFieldDelegate {
    //MARK: - Variables
    let client = Client.getClient()!
    var arrProducts:Array<Product> = []
    var arrStores:Array<Store> = []
    var selectedOption:Int! = 0
    var selectedBtn:UIButton!
    //MARK: - IBOutlet
    @IBOutlet weak var clientPhotoImgView:LoadingImageView!
    @IBOutlet weak var clientNameLbl:UILabel!
    @IBOutlet weak var numberStoresFollowingLbl:UILabel!
    @IBOutlet weak var personalInfoBtn:UIButton!
    @IBOutlet weak var followingStoresBtn:UIButton!
    @IBOutlet weak var personalInfoView:UIView!
    @IBOutlet weak var cellphoneTxtFld:UITextField!
    @IBOutlet weak var emailTxtFld:UITextField!
    @IBOutlet weak var favoriteProductsCollectionView:UICollectionView!
    @IBOutlet weak var followingStoresCollectionView:UICollectionView!
    @IBOutlet weak var heightFollowingStoreCollectionViewLayout:NSLayoutConstraint!
    @IBOutlet weak var bottomSpaceFollowingStoreCollectionViewLayout:NSLayoutConstraint!
    @IBOutlet weak var searchTextFld:UITextField!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        self.arrProducts = Product.getProductsWithProductIds(self.client.products!.componentsSeparatedByString(",").map(){($0 as NSString).integerValue})
        self.arrStores = Store.getStoresWithStoreIds(self.client.stores!.componentsSeparatedByString(",").map(){($0 as NSString).integerValue})
        self.clientPhotoImgView.strUrl = self.client.photo
        self.clientNameLbl.text = self.client.name
        self.numberStoresFollowingLbl.text = "\(self.arrStores.count)"
        self.cellphoneTxtFld.text = self.client.phone
        self.emailTxtFld.text = self.client.email
        self.selectedBtn = self.personalInfoBtn
        let numberRows = ceil(CGFloat(self.arrStores.count) / 3.0)
        self.heightFollowingStoreCollectionViewLayout.constant = numberRows * 90 + (numberRows > 0 ? (numberRows - 1) * 10 : 0)
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let viewCont = segue.destinationViewController as? ProductDetailViewController {
            viewCont.product = self.arrProducts[favoriteProductsCollectionView.indexPathsForSelectedItems()!.first!.item]
        }else if let viewCont = segue.destinationViewController as? StoreViewController {
            viewCont.store = self.arrStores[self.followingStoresCollectionView.indexPathsForSelectedItems()!.first!.item]
        }
    }
    //MARK: - IBAction
    @IBAction func back(){
        self.navigationController?.popViewControllerAnimated(true)
    }
    @IBAction func seeFavorites(){
        if self.navigationController?.viewControllers.first is FavoritesViewController {
            self.navigationController?.popViewControllerAnimated(true)
        }else{
            self.navigationController?.tabBarController?.selectedViewController = self.navigationController?.tabBarController?.viewControllers?.last
        }
    }
    @IBAction func selectOption(btn:UIButton){
        if self.selectedOption == btn.tag {
            return
        }else{
            let previousSelected = self.selectedOption
            self.selectedBtn.setTitleColor(UIColor.darkGrayColor(), forState: .Normal)
            self.selectedBtn = btn
            self.selectedOption = btn.tag
            self.selectedBtn.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            let width = self.view.frame.width
            let duration = 0.25
            if previousSelected == 0 {
                self.followingStoresCollectionView.hidden = false
                self.followingStoresCollectionView.transform = CGAffineTransformMakeTranslation(width, 0)
                self.bottomSpaceFollowingStoreCollectionViewLayout.priority = 200
                UIView.animateWithDuration(duration, delay: 0, options: .CurveEaseInOut, animations: {
                    self.followingStoresCollectionView.transform = CGAffineTransformIdentity
                    self.personalInfoView.transform = CGAffineTransformMakeTranslation(-width, 0)
                    self.view.layoutIfNeeded()
                    }, completion: {(b:Bool) in
                        self.personalInfoView.transform = CGAffineTransformIdentity
                        self.personalInfoView.hidden = true
                })
            }else{
                self.personalInfoView.hidden = false
                self.personalInfoView.transform = CGAffineTransformMakeTranslation(-width, 0)
                self.bottomSpaceFollowingStoreCollectionViewLayout.priority = 100
                UIView.animateWithDuration(duration, delay: 0, options: .CurveEaseInOut, animations: {
                    self.personalInfoView.transform = CGAffineTransformIdentity
                    self.followingStoresCollectionView.transform = CGAffineTransformMakeTranslation(width, 0)
                    self.view.layoutIfNeeded()
                    }, completion: {(b:Bool) in
                        self.followingStoresCollectionView.transform = CGAffineTransformIdentity
                        self.followingStoresCollectionView.hidden = true
                })
            }
        }
    }
    @IBAction func logout(){
        let alertCont = UIAlertController(title: "Alerta", message: "¿Cerrar Sesión?", preferredStyle: .Alert)
        alertCont.addAction(UIAlertAction(title: "Cancelar", style: .Cancel, handler: nil))
        alertCont.addAction(UIAlertAction(title: "Cerrar Sesión", style: .Default, handler: {(action:UIAlertAction) in
            Client.deleteClient()
            Product.deleteProducts()
            self.performSegueWithIdentifier("logout", sender: nil)
        }))
        self.presentViewController(alertCont, animated: true, completion: nil)
    }
    @IBAction func searchProducts(){
        if self.searchTextFld.alpha == 0 {
            UIView.animateWithDuration(0.25, animations: {
                self.searchTextFld.alpha = 1
                }, completion:{(b:Bool) in
                    self.searchTextFld.becomeFirstResponder()
            })
        }else{
            self.searchTextFld.resignFirstResponder()
            UIView.animateWithDuration(0.25, animations: {
                self.searchTextFld.alpha = 0
            })
        }
    }
    //MARK: - CollectionView
    func collectionView(collectionView:UICollectionView, numberOfItemsInSection section:Int) -> Int {
        if collectionView == self.favoriteProductsCollectionView {
            return self.arrProducts.count
        }else{
            return self.arrStores.count
        }
    }
    func collectionView(collectionView:UICollectionView, layout collectionViewLayout:UICollectionViewLayout, sizeForItemAtIndexPath indexPath:NSIndexPath) -> CGSize {
        if collectionView == self.favoriteProductsCollectionView {
            return CGSizeMake(70, 70)
        }else{
            return CGSizeMake(floor(self.view.frame.width / 3), 90)
        }
    }
    func collectionView(collectionView:UICollectionView, cellForItemAtIndexPath indexPath:NSIndexPath) -> UICollectionViewCell {
        if collectionView == self.favoriteProductsCollectionView {
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("favProductCell", forIndexPath: indexPath) as! ProductPhotoCollectionViewCell
            let product = self.arrProducts[indexPath.row]
            cell.productPhotoImgView.strUrl = product.photoURL
            return cell
        }else{
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("storePhotoCell", forIndexPath: indexPath) as! FollowingStoreCollectionViewCell
            let store = self.arrStores[indexPath.item]
            cell.storePhotoImgView.strUrl = store.photoURL
            cell.storeNameLbl.text = store.name
            return cell
        }
    }
    //MARK: - TextField
    func textFieldShouldReturn(textField:UITextField) -> Bool {
        textField.resignFirstResponder()
        if textField == self.searchTextFld {
            if textField.text!.isEmpty {
                UIView.animateWithDuration(0.25, animations: {
                    self.searchTextFld.alpha = 0
                })
            }else{
                self.searchTextFld.alpha = 0
                let viewCont = self.storyboard!.instantiateViewControllerWithIdentifier("productListViewCont") as! ProductListViewController
                viewCont.searchText = textField.text!
                self.navigationController?.pushViewController(viewCont, animated: true)
            }
        }else{
            updateClient()
        }
        return false
    }
    //MARK: - Auxiliar
    func updateClient(){
        let dicParams = ["phone": self.cellphoneTxtFld.text!, "email": self.emailTxtFld.text!]
        ServiceConnector.connectToUrl("\(URLs.updateClientURL)\(self.client.id.integerValue)", params: ToolBox.convertDictionaryToPostParams(dicParams), response: { (result:AnyObject?, error:NSError?) in
            if error == nil {
                if let dicResult = result as? Dictionary<String, AnyObject> {
                    if let state = dicResult["state"] as? String {
                        if state == "success" {
                            self.client.phone = self.cellphoneTxtFld.text
                            self.client.email = self.emailTxtFld.text
                            Client.updateClient()
                            return
                        }else if state == "error" {
                            if let message = dicResult["message"] as? String {
                                ToolBox.showAlertWithTitle("Alerta", withMessage: message, inViewCont: self)
                                return
                            }
                        }
                    }
                }
                ToolBox.showErrorConnectionInViewCont(self)
            }else{
                self.updateClient()
            }
        })
    }
}
