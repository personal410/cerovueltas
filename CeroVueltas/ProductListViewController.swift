//
//  HomeViewController.swift
//  CeroVueltas
//
//  Created by victor salazar on 8/08/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
enum TypeProductListViewCont{
    case home, news, search
}
class ProductListViewController:UIViewController, UITableViewDataSource, UITableViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UITextFieldDelegate {
    //MARK: - Variables
    var arrOptions:Array<FilterOption> = []
    var arrProducts:Array<Product> = []
    var arrFilterProducts:Array<Product> = []
    var arrCurrentProducts:Array<Product> = []
    var productCellHeight:Double = 0
    var productCellWidth:Double = 0
    var currentOptionsList = 0
    var reloadData = true
    var isFiltering = false
    var searchText = ""
    var shouldLoadMore = true
    var currentSelectedGender:Int = -1
    var currentSelectedAge:Int = -1
    var currentSelectedCategoriesOptions:Array<Int> = []
    var currentSelectedPricesOptions:Array<Int> = []
    var dicFilterParams = Dictionary<String, String>()
    //MARK: - IBOutlet
    @IBOutlet weak var searchTextFld:UITextField!
    @IBOutlet weak var filterView:UIView!
    @IBOutlet weak var filterTableView:UITableView!
    @IBOutlet weak var productsCollectionView:UICollectionView!
    @IBOutlet weak var loadingActivityIndicator:UIActivityIndicatorView!
    @IBOutlet weak var backView:UIView!
    @IBOutlet weak var userView:UIView!
    @IBOutlet weak var userImgView:LoadingImageView!
    var type:TypeProductListViewCont = .home
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        if searchText.characters.count == 0 {
            self.type = self.navigationController!.tabBarItem.tag == 0 ? .home : .news
        }else{
            self.type = .search
        }
    }
    override func viewWillAppear(animated:Bool){
        super.viewWillAppear(animated)
        self.productCellWidth = (Double(self.view.frame.width) - 40) / 2.0
        self.productCellHeight = self.productCellWidth * 8.0 / 5.0
        if type == .search {
            self.searchTextFld.alpha = 1
            self.searchTextFld.text = searchText
            self.backView.hidden = false
            self.userView.hidden = true
            let genderParam = ["", "men", "women"][self.currentSelectedGender + 1]
            let ageParam = ["", "kid", "adult"][self.currentSelectedAge + 1]
            let arrCategories = self.currentSelectedCategoriesOptions.map(){"\($0 + 1)"}
            let arrPrices = self.currentSelectedPricesOptions.map(){"\($0 + 1)"}
            dicFilterParams = ["gender": genderParam, "age": ageParam, "categories": arrCategories.joinWithSeparator(","), "price": arrPrices.joinWithSeparator(","), "search": searchText]
            loadFilterProducts()
        }else{
            if reloadData {
                self.reloadData = false
                self.arrProducts = []
                self.arrCurrentProducts = []
                currentSelectedGender = -1
                currentSelectedAge = -1
                currentSelectedCategoriesOptions = []
                currentSelectedPricesOptions = []
                isFiltering = false
                self.productsCollectionView.reloadData()
                self.loadProducts()
            }
            self.productsCollectionView.reloadData()
        }
        if let currentClient = Client.getClient() {
            if let clientPhoto = currentClient.photo {
                self.userImgView.image = nil
                self.userImgView.strUrl = clientPhoto
            }
        }
    }
    override func viewDidDisappear(animated:Bool){
        super.viewDidDisappear(animated)
        self.filterView.hidden = true
    }
    override func shouldPerformSegueWithIdentifier(identifier:String, sender:AnyObject?) -> Bool {
        if Client.getClient() == nil && identifier == "showClient" {
            ToolBox.showNoAccountMessageInViewCont(self)
            return false
        }
        return true
    }
    override func prepareForSegue(segue:UIStoryboardSegue, sender:AnyObject?){
        if let viewCont = segue.destinationViewController as? ProductDetailViewController {
            viewCont.product = self.arrCurrentProducts[self.productsCollectionView.indexPathsForSelectedItems()!.first!.item]
        }else if let viewCont = segue.destinationViewController as? StoreViewController {
            viewCont.store = self.arrCurrentProducts[(sender as! UIButton).tag].store!
        }
    }
    //MARK: - IBAction
    @IBAction func filter(btn:UIButton){
        self.currentOptionsList = btn.tag
        switch self.currentOptionsList {
        case 0:
            self.arrOptions = ToolBox.getGenderOptions()
            break
        case 1:
            self.arrOptions = ToolBox.getAgeOptions()
            break
        case 2:
            self.arrOptions = ToolBox.getCategoriesOptions()
            break
        case 3:
            self.arrOptions = ToolBox.getPriceOptions()
            break
        default:
            self.arrOptions = []
            break
        }
        self.filterTableView.reloadData()
        self.filterView.hidden = false
    }
    @IBAction func searchProducts(){
        if type == .search {
            if self.searchTextFld.isFirstResponder() {
                self.searchTextFld.resignFirstResponder()
            }else{
                self.searchTextFld.becomeFirstResponder()
            }
        }else{
            if self.searchTextFld.alpha == 0 {
                UIView.animateWithDuration(0.25, animations: {
                    self.searchTextFld.alpha = 1
                    }, completion:{(b:Bool) in
                        self.searchTextFld.becomeFirstResponder()
                })
            }else{
                self.searchTextFld.resignFirstResponder()
                UIView.animateWithDuration(0.25, animations: {
                    self.searchTextFld.alpha = 0
                })
            }
        }
    }
    @IBAction func likeProduct(btn:UIButton){
        if Client.getClient() == nil {
            ToolBox.showNoAccountMessageInViewCont(self.navigationController!.tabBarController!)
        }else{
            let product = self.arrCurrentProducts[btn.tag]
            product.isFavorite = !product.isFavorite
            self.productsCollectionView.reloadItemsAtIndexPaths([NSIndexPath(forItem: btn.tag, inSection: 0)])
            let url = "\(URLs.productFavoriteURL)/\(product.id.integerValue)/\(Client.getClient()!.id.integerValue)"
            ServiceConnector.connectToUrl(url, method: "GET"){(result:AnyObject?, error:NSError?) in
                if error == nil {
                    if let dicResult = result as? Dictionary<String, AnyObject> {
                        if let state = dicResult["state"] as? String {
                            if state == "success" {
                                Client.getClient()?.productFavorite(product.id.integerValue, isFavorite: product.isFavorite)
                                return
                            }
                        }
                    }
                }
                product.isFavorite = !product.isFavorite
                self.productsCollectionView.reloadItemsAtIndexPaths([NSIndexPath(forItem: btn.tag, inSection: 0)])
                ToolBox.showAlertWithTitle("Alerta", withMessage: "No se pudo marcar como favorito.", inViewCont: self)
            }
        }
    }
    @IBAction func back(){
        self.navigationController?.popViewControllerAnimated(true)
    }
    @IBAction func showStore(btn:UIButton){
        
    }
    //MARK: - TableView
    func tableView(tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return self.arrOptions.count + 1
    }
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    func tableView(tableView:UITableView, cellForRowAtIndexPath indexPath:NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("optionCell", forIndexPath: indexPath) as! HomeOptionTableViewCell
        if indexPath.row < self.arrOptions.count {
            cell.optionLbl.text = self.arrOptions[indexPath.row].title
            cell.separatorView.hidden = false
            if self.currentOptionsList == 0 {
                cell.checkedImgView.hidden = !(self.currentSelectedGender == indexPath.row)
            }else if self.currentOptionsList == 1 {
                cell.checkedImgView.hidden = !(self.currentSelectedAge == indexPath.row)
            }else if self.currentOptionsList == 2 {
                cell.checkedImgView.hidden = !self.currentSelectedCategoriesOptions.contains(indexPath.row)
            }else{
                cell.checkedImgView.hidden = !self.currentSelectedPricesOptions.contains(indexPath.row)
            }
        }else{
            cell.optionLbl.text = "Aceptar"
            cell.separatorView.hidden = true
            cell.checkedImgView.hidden = true
        }
        return cell
    }
    func tableView(tableView:UITableView, didSelectRowAtIndexPath indexPath:NSIndexPath){
        if indexPath.row == self.arrOptions.count {
            self.filterView.hidden = true
            if self.currentSelectedGender == -1 && self.currentSelectedAge == -1 && self.currentSelectedCategoriesOptions.count == 0 && self.currentSelectedPricesOptions.count == 0  {
                if type == .search {
                    let genderParam = ["", "men", "women"][self.currentSelectedGender + 1]
                    let ageParam = ["", "kid", "adult"][self.currentSelectedAge + 1]
                    let arrCategories = self.currentSelectedCategoriesOptions.map(){"\($0 + 1)"}
                    let arrPrices = self.currentSelectedPricesOptions.map(){"\($0 + 1)"}
                    dicFilterParams = ["gender": genderParam, "age": ageParam, "categories": arrCategories.joinWithSeparator(","), "price": arrPrices.joinWithSeparator(","), "search": searchText]
                    loadingActivityIndicator.startAnimating()
                    self.arrCurrentProducts = []
                    self.productsCollectionView.reloadData()
                    loadFilterProducts()
                }else{
                    self.arrCurrentProducts = self.arrProducts
                    isFiltering = false
                    arrFilterProducts = []
                    shouldLoadMore = true
                    self.productsCollectionView.reloadData()
                }
            }else{
                isFiltering = true
                arrFilterProducts = []
                shouldLoadMore = true
                let genderParam = ["", "men", "women"][self.currentSelectedGender + 1]
                let ageParam = ["", "kid", "adult"][self.currentSelectedAge + 1]
                let arrCategories = self.currentSelectedCategoriesOptions.map(){"\($0 + 1)"}
                let arrPrices = self.currentSelectedPricesOptions.map(){"\($0 + 1)"}
                dicFilterParams = ["gender": genderParam, "age": ageParam, "categories": arrCategories.joinWithSeparator(","), "price": arrPrices.joinWithSeparator(",")]
                if type == .news {
                    dicFilterParams["client_id"] = "\(Client.getClient()!.id)"
                }else if type == .search {
                    dicFilterParams["search"] = searchText
                }
                self.arrCurrentProducts = []
                self.productsCollectionView.reloadData()
                self.loadingActivityIndicator.startAnimating()
                loadFilterProducts()
            }
        }else{
            if self.currentOptionsList == 0 {
                if self.currentSelectedGender == indexPath.row {
                    self.currentSelectedGender = -1
                }else{
                    self.currentSelectedGender = indexPath.row
                }
                tableView.reloadData()
            }else if self.currentOptionsList == 1 {
                if self.currentSelectedAge == indexPath.row {
                    self.currentSelectedAge = -1
                }else{
                    self.currentSelectedAge = indexPath.row
                }
                tableView.reloadData()
            }else if self.currentOptionsList == 2 {
                if let index = self.currentSelectedCategoriesOptions.indexOf(indexPath.row) {
                    self.currentSelectedCategoriesOptions.removeAtIndex(index)
                }else{
                    self.currentSelectedCategoriesOptions.append(indexPath.row)
                }
                tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .None)
            }else{
                if let index = self.currentSelectedPricesOptions.indexOf(indexPath.row) {
                    self.currentSelectedPricesOptions.removeAtIndex(index)
                }else{
                    self.currentSelectedPricesOptions.append(indexPath.row)
                }
                tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .None)
            }
        }
    }
    //MARK: - CollectionView
    func collectionView(collectionView:UICollectionView, numberOfItemsInSection section:Int) -> Int {
        return self.arrCurrentProducts.count
    }
    func collectionView(collectionView:UICollectionView, layout collectionViewLayout:UICollectionViewLayout, sizeForItemAtIndexPath indexPath:NSIndexPath) -> CGSize {
        return CGSize(width: self.productCellWidth, height: self.productCellHeight)
    }
    func collectionView(collectionView:UICollectionView, cellForItemAtIndexPath indexPath:NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("productCell", forIndexPath: indexPath) as! ProductCollectionViewCell
        let product = self.arrCurrentProducts[indexPath.row]
        cell.storeNameBtn.setTitle(product.store?.name, forState: .Normal)
        cell.storeNameBtn.tag = indexPath.item
        cell.productNameLbl.text = product.name
        cell.productImgView.image = nil
        if let photoUrl = product.photoURL {
            cell.productImgView.strUrl = photoUrl
        }
        cell.likeBtn.tag = indexPath.row
        cell.likeBtn.setImage(UIImage(named: product.isFavorite! ? "Like" : "NoLike"), forState: .Normal)
        if shouldLoadMore && indexPath.item + 2 > arrCurrentProducts.count {
            if type == .home || type == .news {
                if isFiltering {
                    loadFilterProducts()
                }else{
                    loadProducts()
                }
            }else{
                loadFilterProducts()
            }
        }
        return cell
    }
    //MARK: - TextField
    func textFieldShouldReturn(textField:UITextField) -> Bool {
        textField.resignFirstResponder()
        if textField.text!.isEmpty {
            UIView.animateWithDuration(0.25, animations: {
                self.searchTextFld.alpha = 0
            })
        }else{
            if type == .search {
                if searchText != searchTextFld.text {
                    searchText = searchTextFld.text!
                    arrFilterProducts = []
                    self.arrCurrentProducts = []
                    productsCollectionView.reloadData()
                    shouldLoadMore = true
                    let genderParam = ["", "men", "women"][self.currentSelectedGender + 1]
                    let ageParam = ["", "kid", "adult"][self.currentSelectedAge + 1]
                    let arrCategories = self.currentSelectedCategoriesOptions.map(){"\($0 + 1)"}
                    let arrPrices = self.currentSelectedPricesOptions.map(){"\($0 + 1)"}
                    dicFilterParams = ["gender": genderParam, "age": ageParam, "categories": arrCategories.joinWithSeparator(","), "price": arrPrices.joinWithSeparator(","), "search": searchText]
                    self.loadingActivityIndicator.startAnimating()
                    loadFilterProducts()
                }
            }else{
                self.searchTextFld.alpha = 0
                let viewCont = self.storyboard!.instantiateViewControllerWithIdentifier("productListViewCont") as! ProductListViewController
                viewCont.searchText = textField.text!
                self.navigationController?.pushViewController(viewCont, animated: true)
            }
        }
        return true
    }
    //MARK: - Auxiliar
    func loadProducts(){
        if arrProducts.count == 0 {
            self.loadingActivityIndicator.startAnimating()
        }
        var url:String = ""
        if type == .home {
            url = "\(URLs.getProductsURL)?count=\(arrProducts.count)"
        }else if type == .news {
            let dicParams = ["client_id": "\(Client.getClient()!.id)", "count": "\(arrProducts.count)"]
            let params = ToolBox.convertDictionaryToPostParams(dicParams)
            url = "\(URLs.getNewsURL)?\(params)"
        }
        ServiceConnector.connectToUrl(url, method: "GET"){(result:AnyObject?, error:NSError?) in
            self.loadingActivityIndicator.stopAnimating()
            if error == nil {
                if let dicResult = result as? Dictionary<String, AnyObject> {
                    if let state = dicResult["state"] as? String {
                        if state == "success" {
                            let arrDicProducts = dicResult["products"] as! Array<Dictionary<String, AnyObject>>
                            if arrDicProducts.count < 10 {
                                self.shouldLoadMore = false
                            }
                            self.arrProducts.appendContentsOf(Product.transformDicProducts(arrDicProducts))
                            self.arrCurrentProducts = self.arrProducts
                            self.productsCollectionView.reloadData()
                            return
                        }else{
                            if let message = dicResult["message"] as? String {
                                ToolBox.showAlertWithTitle("Alerta", withMessage: message, inViewCont: self)
                                return
                            }
                        }
                    }
                }
            }
            ToolBox.showAlertWithTitle("Alerta", withMessage: "No se pudo conectar al servidor", withOkButtonTitle: "Reintentar", withOkHandler: { (alertAction) in
                self.loadProducts()
                }, inViewCont: self)
        }
    }
    func loadFilterProducts(){
        ServiceConnector.connectToUrl("\(URLs.getFilterProductsURL)?count=\(arrFilterProducts.count)", params: ToolBox.convertDictionaryToPostParams(dicFilterParams), response: { (result:AnyObject?, error:NSError?) in
            self.loadingActivityIndicator.stopAnimating()
            if error == nil {
                if let dicResult = result as? Dictionary<String, AnyObject> {
                    let arrDicProducts = dicResult["products"] as! Array<Dictionary<String, AnyObject>>
                    if arrDicProducts.count < 10 {
                        self.shouldLoadMore = false
                    }
                    self.arrFilterProducts.appendContentsOf(Product.transformDicProducts(arrDicProducts))
                    self.arrCurrentProducts = self.arrFilterProducts
                    self.productsCollectionView.reloadData()
                    return
                }
                ToolBox.showErrorConnectionInViewCont(self)
            }else{
                ToolBox.showErrorConnectionInViewCont(self)
            }
        })
    }
}
