//
//  Category.swift
//  CeroVueltas
//
//  Created by victor salazar on 31/08/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import Foundation
import CoreData
class Category:NSManagedObject{
    class func getCategoryWithDic(dicCategory:Dictionary<String, AnyObject>) -> Category {
        let ctxt = AppDelegate.getMoc()
        let fetchReq = NSFetchRequest(entityName: "Category")
        let tempId = (dicCategory["id"] as! NSString).integerValue
        fetchReq.predicate = NSPredicate(format: "id = %i", tempId)
        let results = try! ctxt.executeFetchRequest(fetchReq) as! [Category]
        let category:Category!
        if results.count > 0 {
            category = results.first!
        }else{
            category = NSEntityDescription.insertNewObjectForEntityForName("Category", inManagedObjectContext: ctxt) as! Category
            category.id = tempId
        }
        category.name = dicCategory["category"] as? String
        try! ctxt.save()
        return category
    }
}