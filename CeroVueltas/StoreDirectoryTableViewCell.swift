//
//  StoreDirectoryTableViewCell.swift
//  CeroVueltas
//
//  Created by victor salazar on 28/08/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class StoreDirectoryTableViewCell:UITableViewCell{
    @IBOutlet weak var storeImgView:LoadingImageView!
    @IBOutlet weak var storeNameLbl:UILabel!
    @IBOutlet weak var numberFollowersLbl:UILabel!
    @IBOutlet weak var followBtn:UIButton!
    @IBOutlet weak var followingBtn:UIButton!
}