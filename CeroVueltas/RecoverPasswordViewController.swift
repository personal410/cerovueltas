//
//  RecoverPasswordViewController.swift
//  CeroVueltas
//
//  Created by victor salazar on 9/29/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class RecoverPasswordViewController:UIViewController{
    //MARK: - IBOutlet
    @IBOutlet weak var recoverPasswordView:UIView!
    @IBOutlet weak var emailTxtFld:UITextField!
    //MARK: - IBAction
    @IBAction func dissmiss(){
        NSNotificationCenter.defaultCenter().removeObserver(self)
        emailTxtFld.resignFirstResponder()
        if let viewCont = self.parentViewController as? LoginViewController {
            viewCont.recoverPasswordView.hidden = true
            viewCont.becomeKeyboardObserver()
        }
    }
    @IBAction func recoverPassword(){
        emailTxtFld.resignFirstResponder()
        if emailTxtFld.text!.characters.count == 0 {
            ToolBox.showAlertWithTitle("Alerta", withMessage: "Por favor, ingrese su correo electrónico", inViewCont: self)
        }else{
            SVProgressHUD.showWithStatus("Cargando")
            let dicParams = ["email" : emailTxtFld.text!]
            ServiceConnector.connectToUrl(URLs.recoverPasswordURL, params: ToolBox.convertDictionaryToPostParams(dicParams), response: {(result:AnyObject?, error:NSError?) in
                if error == nil {
                    if let dicResult = result as? Dictionary<String, AnyObject> {
                        if let state = dicResult["state"] as? String {
                            if state == "success" {
                                ToolBox.showAlertWithTitle("Alerta", withMessage: "Le hemos enviado por correo electrónico el enlace para restablecer la contraseña", withOkButtonTitle: "OK", withOkHandler: {(alertAction) in
                                    NSNotificationCenter.defaultCenter().removeObserver(self)
                                    if let viewCont = self.parentViewController as? LoginViewController {
                                        viewCont.recoverPasswordView.hidden = true
                                        viewCont.becomeKeyboardObserver()
                                    }
                                    }, inViewCont: self)
                                return
                            }else if state == "error" {
                                if let message = dicResult["message"] as? String {
                                    ToolBox.showAlertWithTitle("Alerta", withMessage: message, inViewCont: self)
                                    return
                                }
                            }
                        }
                    }
                }
                ToolBox.showErrorConnectionInViewCont(self)
            })
        }
    }
    //MARK: - Keyboard
    func showKeyboard(notification:NSNotification){
        var info = notification.userInfo!
        UIView.animateWithDuration((info[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue, animations: {
            self.recoverPasswordView.transform = CGAffineTransformMakeTranslation(0, -100);
        })
    }
    func hideKeyboard(notification:NSNotification){
        var info = notification.userInfo!
        UIView.animateWithDuration((info[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue, animations: {
            self.recoverPasswordView.transform = CGAffineTransformIdentity;
        })
    }
    //MARK: - Auxiliar
    func becomeKeyboardObserver(){
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RecoverPasswordViewController.showKeyboard(_:)), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RecoverPasswordViewController.hideKeyboard(_:)), name: UIKeyboardWillHideNotification, object: nil)
    }
}
