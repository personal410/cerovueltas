//
//  MapLocalViewController.swift
//  CeroVueltas
//
//  Created by victor salazar on 5/09/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
import GoogleMaps
class MapLocalViewController:UIViewController, UITextFieldDelegate, GMSMapViewDelegate {
    //MARK: - Variables
    var local:Local!
    //MARK: - IBOutlet
    @IBOutlet weak var mapView:GMSMapView!
    @IBOutlet weak var searchTextFld:UITextField!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        let latitude = self.local.latitude
        let longitude = self.local.longitude
        let coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        let camera = GMSCameraPosition.cameraWithTarget(coordinate, zoom: 17)
        self.mapView.camera = camera
        let marker = GMSMarker(position: coordinate)
        marker.map = mapView
        marker.title = "prueba"
        self.mapView.delegate = self
        self.mapView.selectedMarker = marker
    }
    //MARK: - IBAction
    @IBAction func back(){
        self.navigationController?.popViewControllerAnimated(true)
    }
    @IBAction func searchProducts(){
        if self.searchTextFld.alpha == 0 {
            UIView.animateWithDuration(0.25, animations: {
                self.searchTextFld.alpha = 1
                }, completion:{(b:Bool) in
                    self.searchTextFld.becomeFirstResponder()
            })
        }else{
            self.searchTextFld.resignFirstResponder()
            UIView.animateWithDuration(0.25, animations: {
                self.searchTextFld.alpha = 0
            })
        }
    }
    //MARK: - TextField
    func textFieldShouldReturn(textField:UITextField) -> Bool {
        textField.resignFirstResponder()
        if textField.text!.isEmpty {
            UIView.animateWithDuration(0.25, animations: {
                self.searchTextFld.alpha = 0
            })
        }else{
            self.searchTextFld.alpha = 0
            let viewCont = self.storyboard!.instantiateViewControllerWithIdentifier("productListViewCont") as! ProductListViewController
            viewCont.searchText = textField.text!
            self.navigationController?.pushViewController(viewCont, animated: true)
        }
        return true
    }
    //MARK: - MapView
    func mapView(mapView:GMSMapView, markerInfoWindow marker:GMSMarker) -> UIView? {
        let finalView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 40, height: 160))
        finalView.backgroundColor = UIColor.clearColor()
        let contentView = UIView(frame: CGRect(x: 0, y: 0, width: finalView.frame.width, height: finalView.frame.height - 10))
        contentView.backgroundColor = UIColor.whiteColor()
        let bezierPath = UIBezierPath()
        bezierPath.moveToPoint(CGPoint(x: finalView.frame.width / 2 - 9, y: contentView.frame.height))
        bezierPath.addLineToPoint(CGPoint(x: finalView.frame.width / 2 + 9, y: contentView.frame.height))
        bezierPath.addLineToPoint(CGPoint(x: finalView.frame.width / 2, y: contentView.frame.height + 10))
        bezierPath.addLineToPoint(CGPoint(x: finalView.frame.width / 2 - 9, y: contentView.frame.height))
        let triangleLayer = CAShapeLayer()
        triangleLayer.backgroundColor = UIColor.whiteColor().CGColor
        triangleLayer.path = bezierPath.CGPath
        triangleLayer.fillColor = UIColor.whiteColor().CGColor
        finalView.layer.addSublayer(triangleLayer)
        let localImgView = LoadingImageView(frame: CGRect(x: 10, y: 10, width: 32, height: 30))
        localImgView.contentMode = UIViewContentMode.ScaleAspectFill
        localImgView.clipsToBounds = true
        localImgView.strUrl = self.local.store!.photoURL
        contentView.addSubview(localImgView)
        let storeNameLbl = UILabel(frame: CGRect(x: 52, y: 10, width: finalView.frame.width - 62, height: 18))
        storeNameLbl.textColor = UIColor(rgb: 48)
        storeNameLbl.font = UIFont(name: "MuseoSans-500", size: 15)
        storeNameLbl.text = "\(self.local.store!.name!) \(self.local.name!)"
        let storeAddressLbl = UILabel(frame: CGRect(x: 52, y: 28, width: finalView.frame.width - 62, height: 14))
        storeAddressLbl.textColor = UIColor(rgb: 48)
        storeAddressLbl.text = self.local.address
        storeAddressLbl.font = UIFont(name: "MuseoSans-300", size: 11)
        contentView.addSubview(storeAddressLbl)
        let storePhoneLbl = UILabel(frame: CGRect(x: 10, y: 50, width: finalView.frame.width - 20, height: 15))
        storePhoneLbl.textColor = UIColor(rgb: 48)
        storePhoneLbl.text = "Teléfono: \(self.local.store!.phone!)"
        storePhoneLbl.font = UIFont(name: "MuseoSans-300", size: 12)
        contentView.addSubview(storePhoneLbl)
        let storeAttentionLbl = UILabel(frame: CGRect(x: 10, y: 70, width: finalView.frame.width - 20, height: 75))
        storeAttentionLbl.textColor = UIColor(rgb: 48)
        storeAttentionLbl.text = "\(self.local.schedule!)"
        storeAttentionLbl.numberOfLines = 5
        storeAttentionLbl.font = UIFont(name: "MuseoSans-300", size: 12)
        contentView.addSubview(storeAttentionLbl)
        contentView.addSubview(storeNameLbl)
        finalView.addSubview(contentView)
        return finalView
    }
}
