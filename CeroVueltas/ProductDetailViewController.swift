//
//  ProductDetailViewController.swift
//  CeroVueltas
//
//  Created by victor salazar on 16/08/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class ProductDetailViewController:UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    //MARK: - Variables
    var product:Product!
    var selectedBtn:UIButton!
    var selectedOption:Int = 0
    var indexCurrentProduct:Int!
    //MARK: - IBOutlet
    @IBOutlet weak var productImgView:LoadingImageView!
    @IBOutlet weak var priceView:UIView!
    @IBOutlet weak var priceLbl:UILabel!
    @IBOutlet weak var storeNameLbl:UILabel!
    @IBOutlet weak var productNameLbl:UILabel!
    @IBOutlet weak var descriptionLbl:UILabel!
    @IBOutlet weak var characteristicsLbl:UILabel!
    @IBOutlet weak var descriptionBtn:UIButton!
    @IBOutlet weak var availablesColorsBtn:UIButton!
    @IBOutlet weak var likeBtn:UIButton!
    @IBOutlet weak var sizesBtn:UIButton!
    @IBOutlet weak var descriptionView:UIView!
    @IBOutlet weak var availablesColorsCollectionView:UICollectionView!
    @IBOutlet weak var sizesTableView:UITableView!
    @IBOutlet weak var spaceBetweenDescriptionOthersProductsConstraint:NSLayoutConstraint!
    @IBOutlet weak var otherProductsCollectionView:UICollectionView!
    @IBOutlet weak var searchTextFld:UITextField!
    @IBOutlet weak var findingInTableView:UITableView!
    @IBOutlet weak var findingInTableViewHeightConstraing:NSLayoutConstraint!
    @IBOutlet weak var findingInViewHeightConstraing:NSLayoutConstraint!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        self.productImgView.strUrl = product.photoURL
        self.priceLbl.text = "S/. \(product.price)"
        self.storeNameLbl.text = product.store?.name
        self.productNameLbl.text = product.name
        self.descriptionLbl.text = product.productDescription
        self.characteristicsLbl.text = product.characteristics
        self.selectedBtn = self.descriptionBtn
        for i in 0 ..< self.product.store!.products!.array.count {
            let otherProduct = self.product.store!.products!.array[i] as! Product
            if otherProduct.id == product.id {
                indexCurrentProduct = i
                break
            }
        }
        if product.locals!.array.count == 0 {
            findingInViewHeightConstraing.priority = 500
        }else{
            findingInTableViewHeightConstraing.constant = CGFloat(23 * product.locals!.array.count)
        }
    }
    override func viewWillAppear(animated:Bool){
        super.viewWillAppear(animated)
        self.likeBtn.setImage(UIImage(named: product.isFavorite! ? "Like" : "NoLike"), forState: .Normal)
    }
    override func prepareForSegue(segue:UIStoryboardSegue, sender:AnyObject?){
        if let viewCont = segue.destinationViewController as? StoreViewController {
            viewCont.store = self.product.store!
        }else if let viewCont = segue.destinationViewController as? ProductPhotosViewController {
            viewCont.arrPhotos = product.photos!.array as! Array<Photo>
        }else if let viewCont = segue.destinationViewController as? MapLocalViewController {
            viewCont.local = product.locals!.array[(sender as! UIButton).tag] as! Local
        }
    }
    //MARK: - IBAction
    @IBAction func back(){
        self.navigationController?.popViewControllerAnimated(true)
    }
    @IBAction func selectOption(btn:UIButton){
        if selectedOption == btn.tag {
            return
        }
        let previousSelected = self.selectedOption
        self.selectedBtn.setTitleColor(UIColor.darkGrayColor(), forState: .Normal)
        self.selectedBtn = btn
        self.selectedOption = btn.tag
        self.selectedBtn.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        let width = self.view.frame.width
        var duration = 0.25
        if previousSelected == 0 {
            self.availablesColorsCollectionView.transform = CGAffineTransformMakeTranslation(width, 0)
            self.availablesColorsCollectionView.hidden = false
            if selectedOption == 2 {
                self.sizesTableView.transform = CGAffineTransformMakeTranslation(2 * width, 0)
                self.sizesTableView.hidden = false
                duration = duration * 1.5
            }
            self.spaceBetweenDescriptionOthersProductsConstraint.priority = 100
            UIView.animateWithDuration(duration, delay: 0, options: .CurveEaseInOut, animations: {
                self.descriptionView.transform = CGAffineTransformMakeTranslation(-width * CGFloat(self.selectedOption), 0)
                self.availablesColorsCollectionView.transform = CGAffineTransformMakeTranslation(width * CGFloat(1 - self.selectedOption), 0)
                self.sizesTableView.transform = CGAffineTransformIdentity
                self.view.layoutIfNeeded()
                }, completion: {(b:Bool) in
                    self.descriptionView.transform = CGAffineTransformIdentity
                    self.descriptionView.hidden = true
                    if self.selectedOption == 2 {
                        self.availablesColorsCollectionView.transform = CGAffineTransformIdentity
                        self.availablesColorsCollectionView.hidden = true
                    }
            })
        }else if previousSelected == 1 {
            if selectedOption == 0 {
                self.descriptionView.transform = CGAffineTransformMakeTranslation(-width, 0)
                self.descriptionView.hidden = false
                self.spaceBetweenDescriptionOthersProductsConstraint.priority = 200
                UIView.animateWithDuration(duration, delay: 0, options: .CurveEaseInOut, animations: {
                    self.descriptionView.transform = CGAffineTransformIdentity
                    self.availablesColorsCollectionView.transform = CGAffineTransformMakeTranslation(width, 0)
                    self.view.layoutIfNeeded()
                    }, completion: {(b:Bool) in
                        self.availablesColorsCollectionView.transform = CGAffineTransformIdentity
                        self.availablesColorsCollectionView.hidden = true
                })
            }else{
                self.sizesTableView.transform = CGAffineTransformMakeTranslation(width, 0)
                self.sizesTableView.hidden = false
                UIView.animateWithDuration(duration, delay: 0, options: .CurveEaseInOut, animations: {
                    self.availablesColorsCollectionView.transform = CGAffineTransformMakeTranslation(-width, 0)
                    self.sizesTableView.transform = CGAffineTransformIdentity
                    }, completion: {(b:Bool) in
                        self.availablesColorsCollectionView.transform = CGAffineTransformIdentity
                        self.availablesColorsCollectionView.hidden = true
                })
            }
        }else{
            self.availablesColorsCollectionView.transform = CGAffineTransformMakeTranslation(-width, 0)
            self.availablesColorsCollectionView.hidden = false
            if selectedOption == 0 {
                self.descriptionView.transform = CGAffineTransformMakeTranslation(-2 * width, 0)
                self.descriptionView.hidden = false
                duration = duration * 1.5
                self.spaceBetweenDescriptionOthersProductsConstraint.priority = 200
            }
            UIView.animateWithDuration(duration, delay: 0, options: .CurveEaseInOut, animations: {
                self.sizesTableView.transform = CGAffineTransformMakeTranslation(width * CGFloat(2 - self.selectedOption), 0)
                self.availablesColorsCollectionView.transform = CGAffineTransformMakeTranslation(width * CGFloat(1 - self.selectedOption), 0)
                self.descriptionView.transform = CGAffineTransformIdentity
                self.view.layoutIfNeeded()
                }, completion: {(b:Bool) in
                    self.sizesTableView.transform = CGAffineTransformIdentity
                    self.sizesTableView.hidden = true
                    if self.selectedOption == 0 {
                        self.availablesColorsCollectionView.transform = CGAffineTransformIdentity
                        self.availablesColorsCollectionView.hidden = true
                    }
            })
        }
    }
    @IBAction func likeBtn(btn:UIButton){
        if Client.getClient() == nil {
            ToolBox.showNoAccountMessageInViewCont(self.navigationController!.tabBarController!)
        }else{
            product.isFavorite = !product.isFavorite
            self.likeBtn.setImage(UIImage(named: product.isFavorite! ? "Like" : "NoLike"), forState: .Normal)
            let url = "\(URLs.productFavoriteURL)/\(product.id.integerValue)/\(Client.getClient()!.id.integerValue)"
            ServiceConnector.connectToUrl(url, method: "GET"){(result:AnyObject?, error:NSError?) in
                if error == nil {
                    if let dicResult = result as? Dictionary<String, AnyObject> {
                        if let state = dicResult["state"] as? String {
                            if state == "success" {
                                Client.getClient()?.productFavorite(self.product.id.integerValue, isFavorite: self.product.isFavorite)
                                return
                            }
                        }
                    }
                }
                self.product.isFavorite = !self.product.isFavorite
                self.likeBtn.setImage(UIImage(named: self.product.isFavorite! ? "Like" : "NoLike"), forState: .Normal)
                ToolBox.showAlertWithTitle("Alerta", withMessage: "No se pudo marcar como favorito.", inViewCont: self)
            }
        }
    }
    @IBAction func searchProducts(){
        if self.searchTextFld.alpha == 0 {
            UIView.animateWithDuration(0.25, animations: {
                self.searchTextFld.alpha = 1
                }, completion:{(b:Bool) in
                    self.searchTextFld.becomeFirstResponder()
            })
        }else{
            self.searchTextFld.resignFirstResponder()
            UIView.animateWithDuration(0.25, animations: {
                self.searchTextFld.alpha = 0
            })
        }
    }
    @IBAction func showPhotos(){
        print("cant: \(product.photos?.array.count)")
    }
    //MARK: - CollectionView
    func collectionView(collectionView:UICollectionView, numberOfItemsInSection section:Int) -> Int {
        if collectionView == self.availablesColorsCollectionView {
            return self.product.colors!.count
        }else{
            return self.product.store!.products!.array.count - 1
        }
    }
    func collectionView(collectionView:UICollectionView, cellForItemAtIndexPath indexPath:NSIndexPath) -> UICollectionViewCell {
        if collectionView == self.availablesColorsCollectionView {
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("colorCell", forIndexPath: indexPath) as! ColorProductDetailCollectionViewCell
            let color = self.product.colors!.array[indexPath.item] as! Color
            if let hexColor = color.color {
                cell.colorView.backgroundColor = ToolBox.convertHexColorToColor(hexColor.lowercaseString)
            }else {
                cell.colorView.backgroundColor = UIColor.clearColor()
            }
            cell.colorNameLbl.text = color.name
            return cell
        }else{
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("otherProductCell", forIndexPath: indexPath) as! ProductPhotoCollectionViewCell
            cell.productPhotoImgView.image = nil
            var otherProduct:Product?
            if indexPath.item < self.indexCurrentProduct {
                otherProduct = self.product.store!.products!.array[indexPath.item] as? Product
            }else{
                otherProduct = self.product.store!.products!.array[indexPath.item + 1] as? Product
            }
            cell.productPhotoImgView.strUrl = otherProduct?.photoURL
            return cell
        }
    }
    func collectionView(collectionView:UICollectionView, didSelectItemAtIndexPath indexPath:NSIndexPath){
        let otherProductViewCont = self.storyboard!.instantiateViewControllerWithIdentifier("productDetailViewCont") as! ProductDetailViewController
        var otherProduct:Product?
        if indexPath.item < self.indexCurrentProduct {
            otherProduct = self.product.store!.products!.array[indexPath.item] as? Product
        }else{
            otherProduct = self.product.store!.products!.array[indexPath.item + 1] as? Product
        }
        otherProductViewCont.product = otherProduct!
        self.navigationController?.pushViewController(otherProductViewCont, animated: true)
    }
    //MARK: - TableView
    func tableView(tableView:UITableView, heightForHeaderInSection section:Int) -> CGFloat {
        if tableView == sizesTableView {
            return 10
        }else{
            return 0.1
        }
    }
    func tableView(tableView:UITableView, heightForFooterInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        if tableView == sizesTableView {
            return self.product.sizes!.count
        }else{
            return product.locals!.count
        }
    }
    func tableView(tableView:UITableView, cellForRowAtIndexPath indexPath:NSIndexPath) -> UITableViewCell {
        if tableView == sizesTableView {
            let cell = tableView.dequeueReusableCellWithIdentifier("sizeCell", forIndexPath: indexPath) as! SizeProductDetailTableViewCell
            let size = self.product.sizes?.array[indexPath.row] as! Size
            cell.nameSizeLbl.text = size.size
            cell.stockSizeLbl.hidden = true
            return cell
        }else{
            let cell = tableView.dequeueReusableCellWithIdentifier("storeLocalCell", forIndexPath: indexPath) as! StoreLocalTableViewCell
            let local = product.locals!.array[indexPath.row] as! Local
            cell.localNameLbl.text = local.name
            cell.mapBtn.tag = indexPath.row
            return cell
        }
    }
    //MARK: - TextField
    func textFieldShouldReturn(textField:UITextField) -> Bool {
        textField.resignFirstResponder()
        if textField.text!.isEmpty {
            UIView.animateWithDuration(0.25, animations: {
                self.searchTextFld.alpha = 0
            })
        }else{
            self.searchTextFld.alpha = 0
            let viewCont = self.storyboard!.instantiateViewControllerWithIdentifier("productListViewCont") as! ProductListViewController
            viewCont.searchText = textField.text!
            self.navigationController?.pushViewController(viewCont, animated: true)
        }
        return true
    }
}
