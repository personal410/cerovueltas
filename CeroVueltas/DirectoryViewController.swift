//
//  DirectoryViewController.swift
//  CeroVueltas
//
//  Created by victor salazar on 28/08/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class DirectoryViewController:UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    //MARK: - Variables
    var arrStores:Array<Store> = []
    var reloadData = true
    //MARK: - IBOutlet
    @IBOutlet weak var storesTableView:UITableView!
    @IBOutlet var loadingActivityIndicator:UIActivityIndicatorView!
    @IBOutlet weak var searchTextFld:UITextField!
    @IBOutlet weak var userImgView:LoadingImageView!
    //MARK: - ViewCont
    override func viewWillAppear(animated:Bool){
        super.viewWillAppear(animated)
        if self.reloadData {
            self.reloadData = false
            self.arrStores = []
            self.loadingActivityIndicator.startAnimating()
            self.loadDirectory()
        }
        self.storesTableView.reloadData()
        if let currentClient = Client.getClient() {
            if let clientPhoto = currentClient.photo {
                self.userImgView.image = nil
                self.userImgView.strUrl = clientPhoto
            }
        }
    }
    override func shouldPerformSegueWithIdentifier(identifier:String, sender:AnyObject?) -> Bool {
        if Client.getClient() == nil && identifier == "showClient" {
            ToolBox.showNoAccountMessageInViewCont(self)
            return false
        }
        return true
    }
    override func prepareForSegue(segue:UIStoryboardSegue, sender:AnyObject?){
        if let viewCont = segue.destinationViewController as? StoreViewController {
            viewCont.store = self.arrStores[self.storesTableView.indexPathForSelectedRow!.row]
        }
    }
    //MARK: - IBAction
    @IBAction func follow(btn:UIButton){
        if Client.getClient() == nil {
            ToolBox.showNoAccountMessageInViewCont(self.navigationController!.tabBarController!)
        }else{
            let store = self.arrStores[btn.tag]
            let url = "\(URLs.followStoreURL)/\(store.id)/\(Client.getClient()!.id)"
            store.clientFollowing = !store.clientFollowing
            store.followers = store.clientFollowing! ? (store.followers.integerValue + 1) : (store.followers.integerValue - 1)
            self.storesTableView.reloadData()
            ServiceConnector.connectToUrl(url, method: "GET"){(result:AnyObject?, error:NSError?) in
                if error == nil {
                    if let dicResult = result as? Dictionary<String, AnyObject> {
                        if let state = dicResult["state"] as? String {
                            if state == "success" {
                                if store.clientFollowing! {
                                    Client.getClient()?.followStore(Int(store.id))
                                }else{
                                    Client.getClient()?.stopFollowingStore(Int(store.id))
                                }
                                return
                            }
                        }
                    }
                }
                store.clientFollowing = !store.clientFollowing
                store.followers = store.clientFollowing! ? (store.followers.integerValue + 1) : (store.followers.integerValue - 1)
                self.storesTableView.reloadData()
                ToolBox.showAlertWithTitle("Alerta", withMessage: "No se pudo seguir a la tienda.", inViewCont: self)
            }
        }
    }
    @IBAction func searchProducts(){
        if self.searchTextFld.alpha == 0 {
            UIView.animateWithDuration(0.25, animations: {
                self.searchTextFld.alpha = 1
                }, completion:{(b:Bool) in
                    self.searchTextFld.becomeFirstResponder()
            })
        }else{
            self.searchTextFld.resignFirstResponder()
            UIView.animateWithDuration(0.25, animations: {
                self.searchTextFld.alpha = 0
            })
        }
    }
    //MARK: - TableView
    func tableView(tableView:UITableView, heightForHeaderInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(tableView:UITableView, heightForFooterInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return arrStores.count
    }
    func tableView(tableView:UITableView, cellForRowAtIndexPath indexPath:NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("storeCell", forIndexPath: indexPath) as! StoreDirectoryTableViewCell
        let store = arrStores[indexPath.row]
        cell.storeImgView.image = nil
        cell.storeImgView.strUrl = store.photoURL
        cell.storeNameLbl.text = store.name
        cell.numberFollowersLbl.text = "\(store.followers)"
        cell.followBtn.hidden = store.clientFollowing
        cell.followBtn.tag = indexPath.row
        cell.followingBtn.hidden = !store.clientFollowing
        cell.followingBtn.tag = indexPath.row
        if arrStores.count % 10 == 0 && indexPath.row + 2 > arrStores.count {
            loadDirectory()
        }
        return cell
    }
    //MARK: - TextField
    func textFieldShouldReturn(textField:UITextField) -> Bool {
        textField.resignFirstResponder()
        if textField.text!.isEmpty {
            UIView.animateWithDuration(0.25, animations: {
                self.searchTextFld.alpha = 0
            })
        }else{
            self.searchTextFld.alpha = 0
            let viewCont = self.storyboard!.instantiateViewControllerWithIdentifier("productListViewCont") as! ProductListViewController
            viewCont.searchText = textField.text!
            self.navigationController?.pushViewController(viewCont, animated: true)
        }
        return true
    }
    //MARK: - Auxiliar
    func loadDirectory(){
        if arrStores.count == 0 {
            self.loadingActivityIndicator.startAnimating()
        }
        ServiceConnector.connectToUrl("\(URLs.getDirectoryURL)?count=\(arrStores.count)", method: "GET"){(result:AnyObject?, error:NSError?) in
            self.loadingActivityIndicator.stopAnimating()
            if error == nil {
                if let dicResult = result as? Dictionary<String, AnyObject> {
                    if let state = dicResult["state"] as? String {
                        if state == "success" {
                            if let arrDicStores = dicResult["directory"] as? Array<Dictionary<String, AnyObject>> {
                                self.arrStores.appendContentsOf(Store.transformDicStores(arrDicStores))
                                self.storesTableView.reloadData()
                                return
                            }
                        }else{
                            if let message = dicResult["message"] as? String {
                                ToolBox.showAlertWithTitle("Alerta", withMessage: message, inViewCont: self)
                                return
                            }
                        }
                    }
                }
            }
            ToolBox.showAlertWithTitle("Alerta", withMessage: "No se pudo conectar al servidor", withOkButtonTitle: "Reintentar", withOkHandler: { (alertAction) in
                self.loadDirectory()
                }, inViewCont: self)
        }
    }
}
