//
//  LoginViewController.swift
//  CeroVueltas
//
//  Created by victor salazar on 1/08/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
import FBSDKLoginKit
class LoginViewController:UIViewController, UITextFieldDelegate {
    //MARK: - IBOutlet
    @IBOutlet weak var emailTxtFld:UITextField!
    @IBOutlet weak var passwordTxtFld:UITextField!
    @IBOutlet weak var loginView:UIView!
    @IBOutlet weak var recoverPasswordView:UIView!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        becomeKeyboardObserver()
    }
    override func viewDidDisappear(animated:Bool){
        super.viewDidDisappear(animated)
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    //MARK: - IBAction
    @IBAction func close(){
        dismissViewControllerAnimated(true, completion: nil)
    }
    @IBAction func dissmisKeyboard(){
        if !self.emailTxtFld.resignFirstResponder() {
            self.passwordTxtFld.resignFirstResponder()
        }
    }
    @IBAction func login(){
        dissmisKeyboard()
        if self.emailTxtFld.text!.characters.count == 0 || self.passwordTxtFld.text!.characters.count == 0 {
            ToolBox.showAlertWithTitle("Alerta", withMessage: "Todos los campos son requeridos.", inViewCont: self)
        }else{
            let email = self.emailTxtFld.text!
            let password = self.passwordTxtFld.text!
            let dicParams = ["email" : email, "password" : password]
            SVProgressHUD.showWithStatus("Cargando")
            ServiceConnector.connectToUrl(URLs.loginURL, params: ToolBox.convertDictionaryToPostParams(dicParams), response: { (result:AnyObject?, error:NSError?) in
                if error == nil {
                    if let dicResult = result as? Dictionary<String, AnyObject> {
                        if let state = dicResult["state"] as? String {
                            if state == "success" {
                                let dicClient = dicResult["user"] as! Dictionary<String, AnyObject>
                                Client.saveClient(dicClient)
                                self.performSegueWithIdentifier("showUserTabBar", sender: nil)
                                return
                            }else if state == "error" {
                                if let message = dicResult["message"] as? String {
                                    ToolBox.showAlertWithTitle("Alerta", withMessage: message, inViewCont: self)
                                    return
                                }
                            }
                        }
                    }
                }
                ToolBox.showErrorConnectionInViewCont(self)
            })
        }
    }
    @IBAction func fbLogin(){
        let loginMan = FBSDKLoginManager()
        loginMan.logInWithReadPermissions(["public_profile", "email", "user_friends"]){(result:FBSDKLoginManagerLoginResult!, error:NSError!) in
            if error == nil {
                if result.isCancelled {
                    ToolBox.showAlertWithTitle("Alerta", withMessage: "Se cancelo el inicio de sesión con Facebook", inViewCont: self)
                }else{
                    SVProgressHUD.showWithStatus("Cargando")
                    FBSDKGraphRequest.init(graphPath: "/\(result.token.userID)", parameters: ["fields": "id, name, email, gender"], HTTPMethod: "GET").startWithCompletionHandler({(con:FBSDKGraphRequestConnection!, result:AnyObject?, error:NSError?) in
                        if error == nil {
                            if let dicResult = result as? Dictionary<String, AnyObject> {
                                let fbId = dicResult["id"] as! String
                                let dicParams = ["name": dicResult["name"] as! String, "email": dicResult["email"] as! String, "fb_id": fbId, "photo": "https://graph.facebook.com/\(fbId)/picture?type=large"]
                                ServiceConnector.connectToUrl(URLs.fbLoginURL, params: ToolBox.convertDictionaryToPostParams(dicParams), response: { (result:AnyObject?, error:NSError?) in
                                    if error == nil {
                                        if let dicResult = result as? Dictionary<String, AnyObject> {
                                            if let state = dicResult["state"] as? String {
                                                if state == "success" {
                                                    let dicClient = dicResult["user"] as! Dictionary<String, AnyObject>
                                                    Client.saveClient(dicClient)
                                                    self.performSegueWithIdentifier("showUserTabBar", sender: nil)
                                                    return
                                                }else if state == "error" {
                                                    if let message = dicResult["message"] as? String {
                                                        ToolBox.showAlertWithTitle("Alerta", withMessage: message, inViewCont: self)
                                                        return
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    ToolBox.showErrorConnectionInViewCont(self)
                                })
                                return
                            }
                        }
                        SVProgressHUD.dismiss()
                        ToolBox.showErrorConnectionInViewCont(self)
                    })
                }
            }else{
                ToolBox.showErrorConnectionInViewCont(self)
            }
        }
    }
    @IBAction func recoverPassword(){
        recoverPasswordView.hidden = false
        NSNotificationCenter.defaultCenter().removeObserver(self)
        if let viewCont = childViewControllers.first as? RecoverPasswordViewController {
            viewCont.becomeKeyboardObserver()
        }
    }
    //MARK: - Keyboard
    func showKeyboard(notification:NSNotification){
        var info = notification.userInfo!
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as! NSValue).CGRectValue()
        let keyboardHeight = keyboardSize.height
        UIView.animateWithDuration((info[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue, animations: {
            self.loginView.transform = CGAffineTransformMakeTranslation(0, -keyboardHeight)
        })
    }
    func hideKeyboard(notification:NSNotification){
        var info = notification.userInfo!
        UIView.animateWithDuration((info[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue, animations: {
            self.loginView.transform = CGAffineTransformIdentity
        })
    }
    //MARK: - TextField
    func textFieldShouldReturn(textField:UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    //MARK: - Auxiliar
    func becomeKeyboardObserver(){
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LoginViewController.showKeyboard(_:)), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LoginViewController.hideKeyboard(_:)), name: UIKeyboardWillHideNotification, object: nil)
    }
}
