//
//  Photo.swift
//  CeroVueltas
//
//  Created by victor salazar on 31/08/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import Foundation
import CoreData
class Photo:NSManagedObject{
    class func getPhotoWithDic(dicPhoto:Dictionary<String, AnyObject>) -> Photo {
        let ctxt = AppDelegate.getMoc()
        let fetchReq = NSFetchRequest(entityName: "Photo")
        let tempId = (dicPhoto["id"] as! NSString).integerValue
        fetchReq.predicate = NSPredicate(format: "id = %i", tempId)
        let results = try! ctxt.executeFetchRequest(fetchReq) as! [Photo]
        let photo:Photo!
        if results.count > 0 {
            photo = results.first!
        }else{
            photo = NSEntityDescription.insertNewObjectForEntityForName("Photo", inManagedObjectContext: ctxt) as! Photo
            photo.id = tempId
        }
        photo.photo = dicPhoto["photo"] as? String
        photo.photoURL = dicPhoto["photoUrl"] as? String
        photo.principal = (dicPhoto["principal"] as! NSString).integerValue
        photo.productId = (dicPhoto["product_id"] as! NSString).integerValue
        try! ctxt.save()
        return photo
    }
}