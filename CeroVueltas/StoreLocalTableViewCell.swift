//
//  StoreLocalTableViewCell.swift
//  CeroVueltas
//
//  Created by victor salazar on 4/09/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class StoreLocalTableViewCell:UITableViewCell{
    @IBOutlet weak var localNameLbl:UILabel!
    @IBOutlet weak var mapBtn:UIButton!
}