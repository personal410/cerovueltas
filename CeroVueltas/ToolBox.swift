//
//  ToolBox.swift
//  Tu Point
//
//  Created by victor salazar on 19/03/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import Foundation
import UIKit
class ToolBox{
    class func showAlertWithTitle(title:String, withMessage message:String, withOkButtonTitle okButtonTitle:String = "OK", withOkHandler handler:((alertAction:UIAlertAction) -> Void)? = nil, inViewCont viewCont:UIViewController){
        let alertCont = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        alertCont.addAction(UIAlertAction(title: okButtonTitle, style: .Default, handler: handler))
        viewCont.presentViewController(alertCont, animated: true, completion: nil)
    }
    class func showErrorConnectionInViewCont(viewCont:UIViewController){
        self.showAlertWithTitle("Alerta", withMessage: "Hubo error en la conexión.", inViewCont: viewCont)
    }
    class func showNoAccountMessageInViewCont(viewCont:UIViewController){
        let alertCont = UIAlertController(title: "Acceso no autorizado", message: "Si deseas ingresar a esta sección debes tener una cuenta activa.", preferredStyle: .Alert)
        alertCont.addAction(UIAlertAction(title: "Seguir navegando", style: .Default, handler: nil))
        alertCont.addAction(UIAlertAction(title: "Crear cuenta", style: .Default, handler: { (action:UIAlertAction) in
            viewCont.dismissViewControllerAnimated(true, completion: nil)
        }))
        viewCont.presentViewController(alertCont, animated: true, completion: nil)
    }
    class func convertDictionaryToPostParams(dictionary:Dictionary<String, String>) -> String {
        var arrParams = Array<String>()
        for (key, value) in dictionary {
            arrParams.append("\(key)=\(value)")
        }
        return arrParams.joinWithSeparator("&")
    }
    class func convertDateToString(date:NSDate, withFormat format:String = "yyyy-MM-dd HH:mm:00") -> String {
        let dateFormatter = NSDateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "es_ES")
        dateFormatter.dateFormat = format
        return dateFormatter.stringFromDate(date)
    }
    class func convertStringToDate(string:String) -> NSDate {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.dateFromString(string)!
    }
    class func getApplicationDocumentDirectory() -> String{
        return NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0]
    }
    class func getImagesDirectory() -> String {
        return "\(self.getApplicationDocumentDirectory())/images"
    }
    class func getGenderOptions() -> Array<FilterOption> {
        return [FilterOption(nCode: "men", nTitle: "Hombre"), FilterOption(nCode: "women", nTitle: "Mujer")]
    }
    class func getAgeOptions() -> Array<FilterOption> {
        return [FilterOption(nCode: "kid", nTitle: "Niño"), FilterOption(nCode: "adult", nTitle: "Adulto")]
    }
    class func getCategoriesOptions() -> Array<FilterOption> {
        return [FilterOption(nCode: "1", nTitle: "Tops"), FilterOption(nCode: "2", nTitle: "Bottoms"), FilterOption(nCode: "3", nTitle: "Vestidos / enterizos"), FilterOption(nCode: "4", nTitle: "Calzado"), FilterOption(nCode: "5", nTitle: "Carteras / maletas / billeteras"), FilterOption(nCode: "6", nTitle: "Accesorios"), FilterOption(nCode: "7", nTitle: "Otros")]
    }
    class func getPriceOptions() -> Array<FilterOption> {
        return [FilterOption(nCode: "1", nTitle: "S/. 0 - S/. 30"), FilterOption(nCode: "2", nTitle: "S/. 30 - S/. 100"), FilterOption(nCode: "3", nTitle: "S/. 100 - S/. 200"), FilterOption(nCode: "3", nTitle: "S/. 200 - S/. 300"), FilterOption(nCode: "4", nTitle: "S/. 300 - a más")]
    }
    class func convertHexColorToColor(hexColor:String) -> UIColor {
        let strRed = hexColor.substringToIndex(hexColor.startIndex.advancedBy(2))
        let strGreen = hexColor.substringWithRange(hexColor.startIndex.advancedBy(2) ..< hexColor.startIndex.advancedBy(4))
        let strBlue = hexColor.substringFromIndex(hexColor.startIndex.advancedBy(4))
        let red = self.convertHexStringToInt(strRed)
        let green = self.convertHexStringToInt(strGreen)
        let blue = self.convertHexStringToInt(strBlue)
        return UIColor(r: red, g: green, b: blue)
    }
    class func convertHexStringToInt(hexString:String) -> Int {
        let strHexCharacters = "0123456789abcdef"
        let strDecimal = hexString[hexString.startIndex]
        let strUnit = hexString[hexString.startIndex.advancedBy(1)]
        let decimalIndex = strHexCharacters.characters.indexOf(strDecimal)!
        let unitIndex = strHexCharacters.characters.indexOf(strUnit)!
        let decimal = strHexCharacters.startIndex.distanceTo(decimalIndex)
        let unit = strHexCharacters.startIndex.distanceTo(unitIndex)
        return decimal * 16 + unit
    }
}
