//
//  FavoritesViewController.swift
//  CeroVueltas
//
//  Created by victor salazar on 29/08/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class FavoritesViewController:UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    //MARK: - Variables
    var arrFavoriteProducts:Array<Product> = []
    var reloadData = true
    //MARK: - IBOutlet
    @IBOutlet weak var favoriteProductsTableView:UITableView!
    @IBOutlet var loadingActivityIndicator:UIActivityIndicatorView!
    @IBOutlet weak var searchTextFld:UITextField!
    @IBOutlet weak var userImgView:LoadingImageView!
    //MARK: - ViewCont
    override func viewWillAppear(animated:Bool){
        super.viewWillAppear(animated)
        if reloadData {
            reloadData = false
            self.arrFavoriteProducts = []
            self.loadFavorites()
        }
        self.favoriteProductsTableView.reloadData()
        if let currentClient = Client.getClient() {
            if let clientPhoto = currentClient.photo {
                self.userImgView.image = nil
                self.userImgView.strUrl = clientPhoto
            }
        }
    }
    override func prepareForSegue(segue:UIStoryboardSegue, sender:AnyObject?){
        if let viewCont = segue.destinationViewController as? ProductDetailViewController {
            viewCont.product = self.arrFavoriteProducts[self.favoriteProductsTableView.indexPathForSelectedRow!.row]
        }
    }
    //MARK: - IBAction
    @IBAction func likeBtn(btn:UIButton){
        let product = self.arrFavoriteProducts[btn.tag]
        product.isFavorite = !product.isFavorite
        self.favoriteProductsTableView.reloadRowsAtIndexPaths([NSIndexPath(forRow: btn.tag, inSection: 0)], withRowAnimation: .None)
        let url = "\(URLs.productFavoriteURL)/\(product.id.integerValue)/\(Client.getClient()!.id.integerValue)"
        ServiceConnector.connectToUrl(url, method: "GET"){(result:AnyObject?, error:NSError?) in
            if error == nil {
                if let dicResult = result as? Dictionary<String, AnyObject> {
                    if let state = dicResult["state"] as? String {
                        if state == "success" {
                            Client.getClient()?.productFavorite(product.id.integerValue, isFavorite: product.isFavorite)
                            return
                        }
                    }
                }
            }
            product.isFavorite = !product.isFavorite
            self.favoriteProductsTableView.reloadRowsAtIndexPaths([NSIndexPath(forRow: btn.tag, inSection: 0)], withRowAnimation: .None)
            ToolBox.showAlertWithTitle("Alerta", withMessage: "No se pudo marcar como favorito.", inViewCont: self)
        }
    }
    @IBAction func searchProducts(){
        if self.searchTextFld.alpha == 0 {
            UIView.animateWithDuration(0.25, animations: {
                self.searchTextFld.alpha = 1
                }, completion:{(b:Bool) in
                    self.searchTextFld.becomeFirstResponder()
            })
        }else{
            self.searchTextFld.resignFirstResponder()
            UIView.animateWithDuration(0.25, animations: {
                self.searchTextFld.alpha = 0
            })
        }
    }
    //MARK: - TableView
    func tableView(tableView:UITableView, heightForHeaderInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(tableView:UITableView, heightForFooterInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return self.arrFavoriteProducts.count
    }
    func tableView(tableView:UITableView, cellForRowAtIndexPath indexPath:NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("favoriteProductCell", forIndexPath: indexPath) as! FavoriteProductTableViewCell
        let product = self.arrFavoriteProducts[indexPath.row]
        cell.productImgView.image = nil
        cell.productImgView.strUrl = product.photoURL
        cell.productNameLbl.text = product.name
        cell.productBrandLbl.text = product.brand
        cell.storeNameLbl.text = product.store?.name
        cell.bottomLineView.hidden = indexPath.row == (self.arrFavoriteProducts.count - 1)
        cell.likeBtn.tag = indexPath.row
        cell.likeBtn.setImage(UIImage(named: product.isFavorite! ? "Like" : "NoLike"), forState: .Normal)
        return cell
    }
    //MARK: - TextField
    func textFieldShouldReturn(textField:UITextField) -> Bool {
        textField.resignFirstResponder()
        if textField.text!.isEmpty {
            UIView.animateWithDuration(0.25, animations: {
                self.searchTextFld.alpha = 0
            })
        }else{
            self.searchTextFld.alpha = 0
            let viewCont = self.storyboard!.instantiateViewControllerWithIdentifier("productListViewCont") as! ProductListViewController
            viewCont.searchText = textField.text!
            self.navigationController?.pushViewController(viewCont, animated: true)
        }
        return true
    }
    //MARK: - Auxiliar
    func loadFavorites(){
        self.loadingActivityIndicator.startAnimating()
        ServiceConnector.connectToUrl("\(URLs.getFavoritesURL)?client_id=\(Client.getClient()!.id)", method: "GET"){(result:AnyObject?, error:NSError?) in
            self.loadingActivityIndicator.stopAnimating()
            if error == nil {
                if let dicResult = result as? Dictionary<String, AnyObject> {
                    if let state = dicResult["state"] as? String {
                        if state == "success" {
                            let arrDicProducts = dicResult["products"] as! Array<Dictionary<String, AnyObject>>
                            self.arrFavoriteProducts = Product.transformDicProducts(arrDicProducts)
                            let favProducts = self.arrFavoriteProducts.map(){"\($0.id.integerValue)"}
                            Client.getClient()!.products = favProducts.joinWithSeparator(",")
                            Client.updateClient()
                            self.favoriteProductsTableView.reloadData()
                            return
                        }else{
                            if let message = dicResult["message"] as? String {
                                ToolBox.showAlertWithTitle("Alerta", withMessage: message, inViewCont: self)
                                return
                            }
                        }
                    }
                }
            }
            
            ToolBox.showAlertWithTitle("Alerta", withMessage: "No se pudo conectar al servidor", withOkButtonTitle: "Reintentar", withOkHandler: { (alertAction) in
                self.loadFavorites()
                }, inViewCont: self)
        }
    }
}
